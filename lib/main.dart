// import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:planner/presentation/config/router/router.dart';
import 'package:planner/presentation/config/theme/custom_theme.dart';
import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';
import 'package:planner/presentation/modules/calendar/bloc/calendar_bloc.dart';
import 'package:planner/presentation/modules/event/bloc/event_creation_bloc.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';

final GlobalKey<ScaffoldMessengerState> snackBarKey =
    GlobalKey<ScaffoldMessengerState>();

Future main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // Bloc.observer = MyBlocObserver();
  // await Firebase.initializeApp(
  //   options: DefaultFirebaseOptions.currentPlatform,
  // );
  final loginBloc = LoginBloc();
  final calendarBloc = CalendarBloc(loginBloc);
  await dotenv.load(fileName: '.env');
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => loginBloc,
          lazy: false,
        ),
        BlocProvider(
          create: (context) => calendarBloc,
          lazy: false,
        ),
        BlocProvider(
          create: (context) => GroupsBloc(calendarBloc),
          lazy: false,
        ),
        BlocProvider(
          create: (context) => EventCreationBloc(
            calendarBloc,
          ),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp.router(
      scaffoldMessengerKey: snackBarKey,
      title: 'Planner',
      theme: CustomTheme.darkTheme,
      routerConfig: router,
      debugShowCheckedModeBanner: false,
    );
  }
}

// class MyBlocObserver extends BlocObserver {
//   @override
//   void onEvent(Bloc bloc, Object? event) {
//     log(event.toString());
//     super.onEvent(bloc, event);
//   }

//   @override
//   void onChange(BlocBase bloc, Change change) {
//     log('$bloc changed from ${change.currentState} to ${change.nextState}');
//     super.onChange(bloc, change);
//   }
// }
