import 'package:planner/presentation/network/api_client.dart';

class ApiSource {
  var apiClient = ApiClient();

  Future submitEvent(event) async {
    return await apiClient.sendRequest(path: '/event', method: RequestMethod.post, body: event);
  }

  Future editEvent(event) async {
    return await apiClient.sendRequest(path: '/event', method: RequestMethod.put, body: event);
  }

  Future deleteEvent({eventId}) async {
    return await apiClient.sendRequest(
        path: '/event', method: RequestMethod.delete, queryParameters: {'id': eventId});
  }

  Future getAllGroups() async {
    return await apiClient.sendRequest(path: '/groups', method: RequestMethod.get);
  }

  Future getPaginatedGroups({
    required int page,
    required int itemsPerPage,
  }) async {
    return await apiClient.sendRequest(
        path: '/group-list?per_page=$itemsPerPage&page=$page', method: RequestMethod.get);
  }

  Future getAdditional() async {
    return await apiClient.sendRequest(path: '/invitations-list', method: RequestMethod.get);
  }

  Future addGroup(groupName, color, description) async {
    var newGroup = {'name': groupName, 'color': color, 'description': description};
    return await apiClient.sendRequest(path: '/group', method: RequestMethod.post, body: newGroup);
  }

  Future editGroup(group) async {
    return await apiClient.sendRequest(path: '/group', method: RequestMethod.put, body: group);
  }

  Future deleteGroup({groupId}) async {
    return await apiClient.sendRequest(
        path: '/group', method: RequestMethod.delete, queryParameters: {'id': groupId});
  }

  Future getEvents({period, group}) async {
    return await apiClient.sendRequest(
        path: '/events',
        method: RequestMethod.get,
        queryParameters: {'period': period, 'group': group});
  }

  Future shareGroup({groupId, email}) async {
    return await apiClient.sendRequest(
      path: '/group/invite',
      method: RequestMethod.post,
      body: {'group_id': groupId, 'email': email},
    );
  }

  Future leaveGroup({groupId}) async {
    return await apiClient
        .sendRequest(path: '/group/leave', method: RequestMethod.post, body: {'id': groupId});
  }

  Future returnToGroup({groupId}) async {
    return await apiClient
        .sendRequest(path: '/group/back', method: RequestMethod.post, body: {'id': groupId});
  }

  Future handleInvitationGroup({invitationId, accepted}) async {
    return await apiClient.sendRequest(
      path: '/group/handle_invite',
      method: RequestMethod.post,
      body: {'invitation_id': invitationId, 'accepted': accepted},
    );
  }
}
