import 'package:planner/global/model/login_model.dart';
import 'package:planner/global/model/user_sign_up_request.dart';
import 'package:planner/presentation/network/api_client.dart';

abstract class AuthenticationApi {
  Future signIn(LoginModel user);
  Future registerUser(UserSignUpRequest user);
  Future requestVerifyEmailCode({required String email, required String code});
  Future sendCodeAgain({required String email});
}

class PlannerAuthenticationApi extends AuthenticationApi {
  final apiClient = ApiClient();

  @override
  Future signIn(LoginModel user) async {
    return await apiClient.sendRequest(
      path: '/login',
      method: RequestMethod.post,
      body: user.toJson(),
    );
  }

  @override
  Future registerUser(UserSignUpRequest user) async {
    return await apiClient.sendRequest(
      path: '/register',
      method: RequestMethod.post,
      body: user.toJson(),
    );
  }

  @override
  Future sendCodeAgain({required String email}) async {
    return await apiClient.sendRequest(
      path: '/email_verification/send',
      method: RequestMethod.post,
      body: {'email': email},
    );
  }

  @override
  Future requestVerifyEmailCode({required String email, required String code}) async {
    return await apiClient.sendRequest(
      path: '/email_verification/verify',
      method: RequestMethod.post,
      body: {
        'email': email,
        'code': code,
      },
    );
  }

  Future logout() async {
    return await apiClient.sendRequest(
      path: '/logout',
      method: RequestMethod.post,
    );
  }
}
