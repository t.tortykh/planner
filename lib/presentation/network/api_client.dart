import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:planner/global/data/providers/session_data_provider.dart';
import 'package:planner/global/utils/snack_bar.dart';
import 'package:planner/main.dart';

class ApiClient {
  final _sessionTokenProvider = SessionDataProvider();
  final dio = Dio();

  sendRequest({
    required String path,
    required RequestMethod method,
    body,
    header,
    queryParameters,
  }) async {
    var token = await _sessionTokenProvider.getToken().then((value) => value);
    Map<String, dynamic> headers = header ?? (token != null && token != '')
        ? {
            "X-Requested-With": "XMLHttpRequest",
            'Authorization': 'Bearer $token',
          }
        : {
            "X-Requested-With": "XMLHttpRequest",
          };

    dio.options.validateStatus = (status) => true;
    dio.options.baseUrl = dotenv.env['API_URL'] as String;
    dio.options.connectTimeout = const Duration(seconds: 7);
    dio.options.receiveTimeout = const Duration(seconds: 7);
    dio.options.headers = headers;

    Response response;
    if (method == RequestMethod.post) {
      response = await dio.post(path, data: body);
    } else if (method == RequestMethod.get) {
      response =
          await dio.get(path, data: body, queryParameters: queryParameters);
    } else if (method == RequestMethod.put) {
      response = await dio.put(path, data: body);
    } else if (method == RequestMethod.delete) {
      response = await dio.delete(path, queryParameters: queryParameters);
    } else {
      throw Exception('Method not found');
    }

    if (method != RequestMethod.get) {
      snackBarKey.currentState?.hideCurrentSnackBar();
    }

    try {
      if (response.statusCode == 401) {
        // logout locally (delete token)
        // clearUserData();
        throw Exception('Somthing went wrong...401');
      } else if (response.statusCode == 500) {
        throw Exception('Somthing went wrong...500');
      } else if (response.statusCode == 422) {
        throw Exception('Somthing went wrong...422');
      } else if (response.statusCode == 200) {
        return response.data;
      } else {
        throw Exception('Faild connection');
      }
    } catch (e) {
      var msg = response.data['message'] ?? e.toString();
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.error,
        message: msg,
      ));

      return response.data;
    }
  }
}

enum RequestMethod {
  get,
  post,
  put,
  delete,
}
