import 'package:flutter/material.dart';

class AppDrawerButton extends StatelessWidget {
  const AppDrawerButton({
    super.key,
    required this.name,
    required this.icon,
    required this.action,
    required this.withNotification,
  });
  final String name;
  final IconData icon;
  final Function() action;
  final bool withNotification;

//Icons.groups_2_outlined
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 5),
      child: SizedBox(
        width: double.infinity,
        height: 55,
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 40,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 2),
                    color: Colors.white,
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 10,
                          offset: const Offset(2, 2)),
                    ]),
                clipBehavior: Clip.hardEdge,
                child: Row(
                  children: [
                    SizedBox(width: 40, child: Icon(icon)),
                    // const SizedBox(width: 10),
                    Text(
                      name,
                      style: const TextStyle(fontSize: 17, color: Colors.black),
                    )
                  ],
                ),
              ),
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                borderRadius: const BorderRadius.all(Radius.circular(10)),
                onTap: action,
              ),
            ),
            withNotification
                ? Positioned(
                    right: 0,
                    child: Container(
                      width: 20,
                      height: 20,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.red,
                      ),
                    ),
                  )
                : const SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
