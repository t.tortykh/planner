import 'package:flutter/material.dart';
import 'package:planner/global/data/providers/session_data_provider.dart';
import 'package:planner/global/model/group_model.dart';
import 'package:planner/global/model/user.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/presentation/modules/groups/view/groups_screen.dart';
import 'package:planner/presentation/widgets/dialogs/app_bottom_sheet.dart';
import 'package:planner/presentation/modules/groups/components/new_partisipant_form.dart';

class GroupPartisipantsWidget extends StatelessWidget {
  final GroupModel group;
  final bool isExpandable;
  final GroupsType groupsType;
  const GroupPartisipantsWidget({
    super.key,
    required this.group,
    required this.isExpandable,
    required this.groupsType,
  });

  Future<User?> _getUserData() async {
    User? user;
    user = await SessionDataProvider().getUser();
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User?>(
      future: _getUserData(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else {
          final User user = snapshot.data!;
          List<User> participants =
              participantsList(group.participants!, 3, groupsType, user);
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: participants.length,
                // itemExtent: 50,
                itemBuilder: (context, ind) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 25,
                          //TODO: avatar logic?
                          child: participants[ind].iconUrl != null
                              ? Image.network(participants[ind].iconUrl!)
                              : Image.asset('lib/global/assets/user.png'),
                        ),
                        const SizedBox(width: 10),
                        Text(
                          participants[ind].name,
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        const SizedBox(width: 5),
                        ind == 2
                            ? Text(
                                'and ${group.participants!.length - 3} more',
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold),
                              )
                            : const SizedBox.shrink(),
                      ],
                    ),
                  );
                },
              ),
              const SizedBox(height: 7),
              isExpandable
                  ? OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        shape: const StadiumBorder(),
                        padding: const EdgeInsets.only(right: 10),
                      ),
                      onPressed: () => plannerModalBottomSheet(
                        context: context,
                        child: NewPartisipantForm(
                          groupId: group.id,
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const CircleAvatar(
                            backgroundColor: Color.fromARGB(255, 100, 100, 100),
                            radius: 25,
                            child: Icon(
                              Icons.add_circle_outline_sharp,
                              size: 50,
                            ),
                          ),
                          const SizedBox(width: 10),
                          Text(
                            'Add parisipant',
                            style: Theme.of(context).textTheme.bodyMedium,
                          ),
                        ],
                      ),
                    )
                  : const SizedBox.shrink(),
            ],
          );
        }
      },
    );
  }
}
