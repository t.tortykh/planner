import 'package:flutter/material.dart';
import 'package:hsv_color_pickers/hsv_color_pickers.dart';

class ColorPicker extends StatefulWidget {
  final Function(Color) getColor;
  final Color? setColor;
  const ColorPicker({super.key, required this.getColor, this.setColor});

  @override
  ColorPickerState createState() => ColorPickerState();
}

class ColorPickerState extends State<ColorPicker> {
  late HSVColor selectedColor;

  @override
  void initState() {
    if (widget.setColor != null) {
      selectedColor = HSVColor.fromColor(widget.setColor!);
    } else {
      selectedColor = HSVColor.fromColor(const Color(0xff4caf50));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return HuePicker(
      thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 13),
      thumbOverlayColor: selectedColor.toColor(),
      initialColor: selectedColor,
      onChanged: (HSVColor color) {
        setState(
          () {
            widget.getColor(color.toColor());
            selectedColor = color;
          },
        );
      },
    );
  }
}
