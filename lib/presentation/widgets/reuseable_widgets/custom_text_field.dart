import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:planner/global/utils/validation_message.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField(
      {Key? key,
      required this.lable,
      required this.value,
      this.textInputAction,
      this.valueChanged,
      this.validator,
      this.validateMode,
      this.inputFormatters,
      this.focusNode,
      this.autofocus,
      this.keyboardType,
      this.controller,
      this.validationMessage,
      this.inputType,
      this.maxLines,
      this.obscure})
      : super(key: key);
  final String? Function(String?)? validator;
  final String lable;
  final String? value;
  final AutovalidateMode? validateMode;
  final TextInputType? keyboardType;
  final bool? autofocus;
  final FocusNode? focusNode;
  final List<TextInputFormatter>? inputFormatters;
  final Function(String)? valueChanged;
  final TextInputAction? textInputAction;
  final TextEditingController? controller;
  final Map<String, dynamic>? validationMessage;
  final String? inputType;
  final int? maxLines;
  final bool? obscure;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  late List? errorMsg;
  late bool? obscure;

  @override
  void initState() {
    if (widget.validationMessage != null && widget.inputType != null) {
      errorMsg = defineError(
          validationMsges: widget.validationMessage!, type: widget.inputType!);
      if (errorMsg!.isEmpty) {
        errorMsg = null;
      }
    } else {
      errorMsg = null;
    }
    obscure = widget.obscure;
    super.initState();
  }

  cleanValidation(validationMessage) {
    setState(() {
      errorMsg = null;
    });
  }

  showObscureText() {
    setState(() {
      obscure = !obscure!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        TextFormField(
          maxLines: widget.maxLines ?? 1,
          autofocus: widget.autofocus ?? false,
          initialValue: widget.value == 'null' ? '' : widget.value,
          textInputAction: widget.textInputAction,
          autovalidateMode: widget.validateMode,
          inputFormatters: widget.inputFormatters,
          controller: widget.controller,
          keyboardType: widget.keyboardType,
          obscureText: obscure ?? false,
          focusNode: widget.focusNode,
          decoration: _inputDecoration(),
          style: const TextStyle(color: Colors.white),
          onChanged: (value) {
            if (value != '' && widget.valueChanged != null) {
              widget.valueChanged!(value);
            }
            if (errorMsg != null) {
              cleanValidation(errorMsg);
            }
          },
          onSaved: (value) {
            if (value != null && widget.valueChanged != null) {
              widget.valueChanged!(value);
            }
          },
          validator: widget.validator,
        ),
        if (errorMsg != null)
          Column(
            children: errorMsg!.map((message) {
              return Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: const EdgeInsets.only(top: 5, left: 8),
                  child: Text(
                    message,
                    style: const TextStyle(
                      color: Colors.red,
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
      ],
    );
  }

  InputDecoration _inputDecoration() {
    return InputDecoration(
      suffixIcon: obscure != null
          // && widget.obscure == true
          ? IconButton(
              onPressed: showObscureText,
              icon: Icon(
                obscure! ? Icons.visibility_off : Icons.visibility,
                color: Colors.white,
              ),
            )
          : null,
      labelText: widget.lable,
      labelStyle: const TextStyle(
        color: Color.fromARGB(255, 191, 121, 121),
        fontWeight: FontWeight.bold,
        fontSize: 15,
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(
            color: errorMsg == null
                ? const Color.fromARGB(255, 191, 121, 121)
                : Colors.red,
            width: 3),
      ),
      focusedBorder: const UnderlineInputBorder(
        borderSide:
            BorderSide(color: Color.fromARGB(255, 191, 121, 121), width: 3),
      ),
      contentPadding: const EdgeInsets.all(10),
    );
  }
}
