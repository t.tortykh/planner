import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../global/utils/utils.dart';

// ignore: must_be_immutable
class CustomDateTimePicker extends StatefulWidget {
  late DateTime selectedDate;
  late TimeOfDay selectedTime;
  CustomDateTimePicker({
    super.key,
    required this.lable,
    required this.initDate,
    required this.valueChanged,
    required this.onlyDate,
  }) {
    selectedDate = initDate;
    selectedTime = roundTime(TimeOfDay.fromDateTime(initDate));
  }

  final bool onlyDate;
  final String lable;
  final DateTime initDate;
  final Function(DateTime) valueChanged;

  @override
  State<CustomDateTimePicker> createState() => _CustomDateTimePickerState();
}

class _CustomDateTimePickerState extends State<CustomDateTimePicker> {
  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate: widget.selectedDate,
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    );
    if (picked != null && picked != widget.selectedDate) {
      setState(() {
        widget.selectedDate = picked;
      });
    }
    _getValue();
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: widget.selectedTime,
    );

    if (picked != null && picked != widget.selectedTime) {
      setState(() {
        widget.selectedTime = picked;
      });
    }
    _getValue();
  }

  _getValue() {
    String time;
    if (widget.onlyDate) {
      time = '00:00:00';
    } else {
      time =
          '${widget.selectedTime.hour.toString().padLeft(2, '0')}:${widget.selectedTime.minute.toString().padLeft(2, '0')}:00';
    }

    var date = DateFormat('yyyy-MM-dd').format(widget.selectedDate);
    var result = '$date $time';

    widget.valueChanged(parseDate(result));
  }

  @override
  Widget build(BuildContext context) {
    final String time = widget.selectedTime.format(context);
    return Center(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              widget.lable,
              style: const TextStyle(
                  fontSize: 16,
                  color: Color.fromARGB(255, 191, 121, 121),
                  fontWeight: FontWeight.bold),
            ),
          ),
          ElevatedButton(
            style: dateTimeButtonStyle(),
            onPressed: () {
              _selectDate(context);
            },
            child: Text(
              DateFormat('dd/MM/yyyy').format(widget.selectedDate),
            ),
          ),
          const SizedBox(width: 16),
          widget.onlyDate
              ? const SizedBox()
              : ElevatedButton(
                  style: dateTimeButtonStyle(),
                  onPressed: () {
                    _selectTime(context);
                  },
                  child: Text(time),
                ),
        ],
      ),
    );
  }
}
