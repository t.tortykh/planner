import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';
import 'package:planner/presentation/widgets/avatar_widget.dart';
import 'package:planner/presentation/widgets/app_drawer_button.dart';
import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    final loginBloc = context.read<LoginBloc>();
    final groupState = context.select((GroupsBloc bloc) => bloc.state);

    return Drawer(
      elevation: 50,
      backgroundColor: const Color.fromARGB(255, 113, 180, 188),
      shadowColor: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(180),
        ),
      ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Expanded(
                    flex: 1,
                    child: AvatarWidget(),
                  ),
                  Expanded(
                    flex: 3,
                    child: Text(
                      loginBloc.state.userName ?? 'Dear friend',
                      style: Theme.of(context).textTheme.titleMedium,
                      maxLines: 3,
                      overflow: TextOverflow.clip,
                    ),
                  )
                ],
              ),
              const Divider(),
              Expanded(
                child: Column(
                  children: [
                    AppDrawerButton(
                      name: 'Groups',
                      icon: Icons.groups_2_outlined,
                      withNotification: groupState.invitationsList.isNotEmpty ? true : false,
                      action: () => context.goNamed('groups'),
                    ),
                  ],
                ),
              ),
              Image.asset('lib/global/assets/planner.png', scale: 7),
            ],
          ),
        ),
      ),
    );
  }
}
