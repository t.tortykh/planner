import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:planner/presentation/widgets/custom_avatar.dart';
import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';

class AvatarWidget extends StatelessWidget {
  const AvatarWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final loginBloc = context.read<LoginBloc>();
    return CustomAvatar(
      action: () async {
        loginBloc.add(UserLogoutEvent());
      },
      actionButtonName: 'Log out',
      title: 'Are you shure you want to log out?',
    );
  }
}
