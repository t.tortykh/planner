import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planner/global/model/event_model.dart';

class EventDateColumn extends StatelessWidget {
  const EventDateColumn({
    super.key,
    required this.event,
  });

  final CalendarEventModel event;

  String getDateTime(String returnedType, String date) {
    return DateFormat(returnedType).format(DateTime.parse(date));
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            getDateTime('dd MM yy', event.dateStart!) != getDateTime('dd MM yy', event.dateEnd!)
                ? Row(
                    children: [
                      Text(
                        getDateTime('dd', event.dateStart!),
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                      Text(
                        '-',
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                      Text(
                        getDateTime('dd', event.dateEnd!),
                        style: Theme.of(context).textTheme.labelLarge,
                      ),
                    ],
                  )
                : Text(
                    getDateTime('dd', event.dateStart!),
                    style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 40,
                    ),
                  ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  getDateTime('MMMM', event.dateStart!),
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                const Text(' \''),
                Text(DateFormat('yy').format(DateTime.parse(event.dateStart!))),
              ],
            ),
            const SizedBox(height: 40),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 4),
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(7.0),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    DateFormat('H:mm').format(
                      DateTime.parse(event.dateStart!),
                    ),
                    style: const TextStyle(color: Colors.white),
                  ),
                  const Text('-'),
                  Text(
                    DateFormat('H:mm').format(
                      DateTime.parse(event.dateEnd!),
                    ),
                    style: const TextStyle(color: Colors.white),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
