import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/custom_text_field.dart';

class CodeFieldWidget extends StatefulWidget {
  const CodeFieldWidget({
    required this.code,
    super.key,
    this.callback,
    required this.collectCode,
  });
  final List code;
  final void Function(dynamic)? callback;
  final Function collectCode;

  @override
  State<CodeFieldWidget> createState() => _CodeFieldWidgetState();
}

class _CodeFieldWidgetState extends State<CodeFieldWidget> {
  late FocusNode firstFocus;
  late FocusNode secondFocus;
  late FocusNode thirdFocus;
  late FocusNode forthFocus;

  @override
  void initState() {
    super.initState();
    firstFocus = FocusNode();
    secondFocus = FocusNode();
    thirdFocus = FocusNode();
    forthFocus = FocusNode();
  }

  @override
  void dispose() {
    firstFocus.dispose();
    secondFocus.dispose();
    thirdFocus.dispose();
    forthFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(40.0),
      child: Row(
        children: [
          CodeCell(
            key: UniqueKey(),
            index: 0,
            collectCode: widget.collectCode,
            focusNode: firstFocus,
            nextFocusNode: secondFocus,
            value: widget.code.isNotEmpty ? widget.code[0].toString() : '',
            callback: widget.callback,
          ),
          const SizedBox(width: 15),
          CodeCell(
            key: UniqueKey(),
            index: 1,
            collectCode: widget.collectCode,
            focusNode: secondFocus,
            nextFocusNode: thirdFocus,
            value: widget.code.length > 1 ? widget.code[1].toString() : '',
            callback: widget.callback,
          ),
          const SizedBox(width: 15),
          CodeCell(
            key: UniqueKey(),
            index: 2,
            collectCode: widget.collectCode,
            focusNode: thirdFocus,
            nextFocusNode: forthFocus,
            value: widget.code.length > 2 ? widget.code[2].toString() : '',
            callback: widget.callback,
          ),
          const SizedBox(width: 15),
          CodeCell(
            key: UniqueKey(),
            index: 3,
            collectCode: widget.collectCode,
            focusNode: forthFocus,
            value: widget.code.length > 3 ? widget.code[3].toString() : '',
            callback: widget.callback,
          ),
        ],
      ),
    );
  }
}

class CodeCell extends StatelessWidget {
  const CodeCell({
    super.key,
    required this.focusNode,
    this.nextFocusNode,
    required this.value,
    this.callback,
    required this.collectCode,
    required this.index,
  });
  final Function collectCode;
  final Function(dynamic)? callback;
  final FocusNode focusNode;
  final FocusNode? nextFocusNode;
  final String value;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        return Flexible(
          child: CustomTextField(
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            textInputAction: TextInputAction.next,
            keyboardType: TextInputType.number,
            lable: '',
            value: value,
            focusNode: focusNode,
            valueChanged: (value) {
              if (value.length == 1) {
                collectCode(index: index, value: value);

                focusNode.unfocus();
                if (nextFocusNode != null) {
                  nextFocusNode!.requestFocus();
                }
              } else if (value.length > 1) {
                var pastedValue = value;
                if (pastedValue.length > 4) {
                  pastedValue = pastedValue.substring(value.length - 4);
                }

                var pastedCode = [];
                for (var rune in pastedValue.runes) {
                  String? character = String.fromCharCode(rune);
                  pastedCode.add(character);
                }

                if (callback != null) {
                  callback!(pastedCode);
                }
              }
            },
          ),
        );
      },
    );
  }
}
