import 'package:flutter/material.dart';

class LoginHeadder extends StatelessWidget {
  const LoginHeadder({
    required this.title,
    super.key,
  });
  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Center(
            child: Text(
              title,
              style:
                  const TextStyle(fontSize: 40, color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Image.asset('lib/global/assets/planner.png', scale: 7)
      ],
    );
  }
}
