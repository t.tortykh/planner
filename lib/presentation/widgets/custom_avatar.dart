import 'package:flutter/material.dart';

class CustomAvatar extends StatelessWidget {
  // final String? avatarPath;
  final String? title;
  final String? actionButtonName;
  final Function()? action;
  // final bool loggedIn;

  const CustomAvatar({
    Key? key,
    this.title,
    this.actionButtonName,
    this.action,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const String standartAvatar = 'https://cdn-icons-png.flaticon.com/128/2102/2102633.png';
    return Stack(
      children: [
        CircleAvatar(
          radius: 23,
          backgroundColor: Colors.white,
          child: Container(
            margin: const EdgeInsets.all(3.0),
            child: ClipOval(
              child:
                  // avatarPath != null && avatarPath != ''
                  //     ? Image.network(
                  //         avatarPath!,
                  //         fit: BoxFit.fill,
                  //       )
                  //     :
                  Image.network(
                standartAvatar,
                fit: BoxFit.fill,
              ),
            ),
          ),
        ),
        Positioned.fill(
          child: Material(
            color: const Color.fromARGB(0, 163, 55, 55),
            child: InkResponse(
                radius: 23,
                splashColor: const Color.fromARGB(124, 197, 180, 129),
                onTap: action != () {}
                    ? () {
                        //   //todo change logic -> navigate to settings(create) -> logout

                        //   if (action != () {} ||
                        //       action != null ||
                        //       actionButtonName != null ||
                        //       title != null) {
                        showAdaptiveDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog.adaptive(
                              title: Text(actionButtonName!),
                              content: Text(title!),
                              actions: [
                                TextButton(
                                  onPressed: () {
                                    action!();
                                    Navigator.pop(context);
                                  },
                                  child: Text(actionButtonName!),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: const Text('Cancel'),
                                )
                              ],
                            );
                          },
                        );
                      }
                    // } else {
                    //   return;
                    // }
                    : () {}),
          ),
        )
      ],
    );
  }
}

//import 'package:flutter/material.dart';

// class CustomAvatar extends StatelessWidget {
//   final String? avatarPath;
//   final Function() onTap;

//   const CustomAvatar({
//     Key? key,
//     required this.avatarPath,
//     required this.onTap,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     const String standartAvatar = 'https://cdn-icons-png.flaticon.com/128/2102/2102633.png';
//     return Stack(
//       children: [
//         CircleAvatar(
//           radius: 23,
//           backgroundColor: Colors.white,
//           child: Container(
//             margin: const EdgeInsets.all(3.0),
//             child: ClipOval(
//               child: avatarPath != null && avatarPath != ''
//                   ? Image.network(
//                       avatarPath!,
//                       fit: BoxFit.fill,
//                     )
//                   : Image.network(
//                       standartAvatar,
//                       fit: BoxFit.fill,
//                     ),
//             ),
//           ),
//         ),
//         Positioned.fill(
//           child: Material(
//             color: const Color.fromARGB(0, 163, 55, 55),
//             child: InkResponse(
//               radius: 23,
//               splashColor: const Color.fromARGB(124, 197, 180, 129),
//               onTap: () {
//                 onTap();
//               },
//             ),
//           ),
//         )
//       ],
//     );
//   }
// }
