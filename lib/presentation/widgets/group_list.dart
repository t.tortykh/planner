import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:planner/global/model/group_model.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';

class GroupListWidget extends StatefulWidget {
  final List<GroupModel> groups;
  final dynamic Function(int?) action;
  final dynamic nextPaginationPage;

  const GroupListWidget({
    super.key,
    required this.groups,
    required this.action,
    required this.nextPaginationPage,
  });

  @override
  State<GroupListWidget> createState() => _GroupListWidgetState();
}

class _GroupListWidgetState extends State<GroupListWidget> {
  int activeButton = 0;

  final scrollController = ScrollController();

  void setupScrollController() {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<GroupsBloc>(context).add(UpdateGroupsEvent());
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();
    setupScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 55,
        child: ListView.builder(
          controller: scrollController,
          scrollDirection: Axis.horizontal,
          itemCount: widget.groups.length,
          itemBuilder: ((context, index) {
            var colorGroup = widget.groups[index].color;
            colorGroup != null ? parseColor(colorGroup) : null;
            if (index == widget.groups.length - 1 &&
                widget.nextPaginationPage != null &&
                index > 9) {
              return const Center(
                child: CircularProgressIndicator(strokeWidth: 2),
              );
            }
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 7, vertical: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                boxShadow: [
                  BoxShadow(
                    color: activeButton == index
                        ? colorGroup != null
                            ? parseColor(colorGroup).withOpacity(0.65)
                            : Colors.grey.withOpacity(0.5)
                        : Colors.black,
                    offset: const Offset(0, 0),
                    blurRadius: 8.0,
                  ),
                ],
              ),
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    activeButton = index;
                    widget.action(widget.groups[index].id);
                  });
                },
                style: ElevatedButton.styleFrom(
                  padding:
                      const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                  shape: const RoundedRectangleBorder(
                    side: BorderSide(
                        width: 2, color: Color.fromARGB(255, 195, 194, 194)),
                    borderRadius: BorderRadius.all(
                      Radius.circular(10.0),
                    ),
                  ),
                  backgroundColor:
                      colorGroup != null ? parseColor(colorGroup) : Colors.grey,
                ),
                child: Text(
                  widget.groups[index].name ?? '',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ),
            );
          }),
        ),
      ),
    );
  }
}
