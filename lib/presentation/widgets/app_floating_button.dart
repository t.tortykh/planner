import 'package:flutter/material.dart';

class AppFloatingButton extends StatelessWidget {
  const AppFloatingButton(
      {super.key, required this.buttonName, required this.onPressed});

  final String buttonName;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 170,
      child: FloatingActionButton(
        backgroundColor: const Color.fromARGB(248, 48, 139, 95),
        shape: const RoundedRectangleBorder(
          side: BorderSide(width: 3, color: Color.fromARGB(255, 195, 194, 194)),
          borderRadius: BorderRadius.all(
            Radius.circular(25.0),
          ),
        ),
        onPressed: onPressed,
        child: Text(buttonName),
      ),
    );
  }
}
