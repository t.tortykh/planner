import 'package:flutter/material.dart';

Future<void> plannerModalBottomSheet({required BuildContext context, required Widget child}) async {
  return await showModalBottomSheet(
    backgroundColor: const Color.fromARGB(221, 66, 66, 66),
    barrierColor: Colors.black54,
    isScrollControlled: true,
    context: context,
    builder: (context) {
      return SingleChildScrollView(
        child: SafeArea(
          child: Container(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
              child: child,
            ),
          ),
        ),
      );
    },
  );
}
