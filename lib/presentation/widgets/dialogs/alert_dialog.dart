import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Future<dynamic> showAlertDialog(
  BuildContext context, {
  String? content,
  required String title,
  required String actionButtonName,
  required Function() action,
}) {
  return showAdaptiveDialog(
    context: context,
    builder: (ctx) => AlertDialog.adaptive(
      content: content != null ? Text(content) : const SizedBox(),
      title: Text(title),
      actions: Platform.isIOS
          ? [
              CupertinoButton(
                child: Text(actionButtonName),
                onPressed: () {
                  action();
                  Navigator.pop(ctx);
                },
              ),
              CupertinoButton(
                  onPressed: () {
                    Navigator.pop(ctx);
                  },
                  child: const Text('Cancel'))
            ]
          : [
              ElevatedButton(
                onPressed: () {
                  action();
                  Navigator.pop(ctx);
                },
                child: Text(actionButtonName),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(ctx);
                },
                child: const Text('Cancel'),
              )
            ],
    ),
  );
}
