import 'package:flutter/material.dart';

class TextCardBloc extends StatelessWidget {
  const TextCardBloc(
      {super.key, required this.title, this.severalLinesText, this.list, this.oneLineText});
  final String title;
  final String? severalLinesText;
  final List<String>? list;
  final String? oneLineText;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Text(
            title,
            // style: ,
          ),
          const SizedBox(height: 5),
          if (severalLinesText != null)
            SizedBox(
              height: 100,
              child: Scrollbar(
                child: SingleChildScrollView(
                  child: Text(
                    severalLinesText!,
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                ),
              ),
            ),
          oneLineText != null
              ? Text(
                  oneLineText!,
                  style: Theme.of(context).textTheme.titleSmall,
                )
              : const SizedBox(),
          if (list != null)
            SizedBox(
              height: 100,
              child: Scrollbar(
                child: SingleChildScrollView(
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: list?.length,
                    itemBuilder: ((context, index) {
                      return Center(
                          child: Text(
                        '- ${list![index]}',
                        style: Theme.of(context).textTheme.titleSmall,
                      ));
                    }),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}
