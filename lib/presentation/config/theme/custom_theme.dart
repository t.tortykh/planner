import 'package:flutter/material.dart';

class CustomTheme {
  static ThemeData get darkTheme {
    return ThemeData.dark(
            // colorScheme: ColorScheme.fromSeed(
            //   seedColor: Color.fromRGBO(74, 142, 164, 0),
            // ),
            )
        .copyWith(
      textTheme: const TextTheme().copyWith(
        titleLarge: const TextStyle(
          fontSize: 24,
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
        titleMedium: const TextStyle(
          color: Colors.white,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
        titleSmall: const TextStyle(
          color: Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.bold,
        ),
        bodySmall: const TextStyle(
          color: Colors.black45,
          fontSize: 13,
          fontWeight: FontWeight.bold,
        ),
        bodyMedium: const TextStyle(
          color: Color.fromRGBO(165, 165, 165, 1),
          fontSize: 16,
        ),
        bodyLarge: const TextStyle(
          color: Colors.black54,
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
      ),
      scaffoldBackgroundColor: Colors.black,
      appBarTheme: const AppBarTheme().copyWith(
        titleTextStyle:
            const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        elevation: 0,
        color: Colors.black,
      ),
      iconButtonTheme: const IconButtonThemeData(
        style: ButtonStyle(
          iconColor: MaterialStatePropertyAll(
            Color.fromARGB(255, 255, 255, 255),
          ),
        ),
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(
            const Color.fromARGB(248, 48, 139, 95),
          ),
          textStyle: MaterialStateProperty.all(
            const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          padding: MaterialStateProperty.all(
            const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 35,
            ),
          ),
          shape: MaterialStateProperty.all(
            const RoundedRectangleBorder(
              side: BorderSide(
                width: 3,
                color: Color.fromARGB(255, 195, 194, 194),
              ),
              borderRadius: BorderRadius.all(
                Radius.circular(25.0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
