import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/presentation/modules/auth/login/view/login_screen.dart';
import 'package:planner/presentation/modules/auth/register/bloc/registration_bloc.dart';
import 'package:planner/presentation/modules/auth/register/view/email_veryfication_screen.dart';
import 'package:planner/presentation/modules/auth/register/view/register_screen.dart';
import 'package:planner/presentation/modules/error/error_screen.dart';
import 'package:planner/presentation/modules/event/view/event_card.dart';
import 'package:planner/presentation/modules/event/view/event_form.dart';
import 'package:planner/presentation/modules/groups/view/group_card_screen.dart';
import 'package:planner/presentation/modules/groups/view/groups_screen.dart';
import 'package:planner/presentation/modules/home/main_screen.dart';

GoRouter router = GoRouter(
  observers: [
    // GoRouterObserver(),
  ],
  initialLocation: '/',
  routes: [
    GoRoute(
      path: '/',
      name: 'main_screen',
      builder: (context, state) {
        FlutterNativeSplash.remove();
        return const MainScreen();
      },
      routes: [
        GoRoute(
          name: 'new_event',
          path: 'new_event',
          builder: (context, state) {
            final data = state.extra as Map<String, dynamic>;
            return EventScreenForm(
              selectedDay: data['selectedDay'],
            );
          },
        ),
        GoRoute(
          name: 'login',
          path: 'login',
          builder: (context, state) => const LoginScreen(),
          routes: [
            GoRoute(
              name: 'registration',
              path: 'registration',
              builder: (context, state) {
                return BlocProvider(
                  create: (context) => RegistrationBloc(),
                  child: const RegisterScreen(),
                );
              },
              routes: [
                GoRoute(
                    path: 'email_varification',
                    name: 'email_varification',
                    builder: (context, state) {
                      return const EmailVeryficationScreen();
                    })
              ],
            ),
          ],
        ),
        GoRoute(
          name: 'event_card',
          path: 'event_card/:id',
          builder: (context, state) {
            return const EventCardScreen();
          },
          routes: [
            GoRoute(
              name: 'event_form',
              path: 'edit',
              builder: (context, state) {
                return const EventScreenForm();
              },
            ),
          ],
        ),
        GoRoute(
          name: 'groups',
          path: 'groups',
          builder: (context, state) => const GroupsScreen(),
          routes: [
            GoRoute(
              name: 'group_card',
              path: 'group_card',
              builder: (context, state) {
                final data = state.extra as Map<String, dynamic>;
                return GroupCardScreen(
                  group: data['group'],
                  groupsType: data['groupsType'],
                );
              },
            ),
          ],
        ),
        GoRoute(
          path: 'error',
          name: 'error',
          builder: (context, state) => const ErrorScreen(),
        ),
      ],
    ),
  ],
);

// class GoRouterObserver extends NavigatorObserver {
//   @override
//   void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
//     log('MyTest didPush: $route');
//   }

//   @override
//   void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
//     log('MyTest didPop: $route');
//   }

//   @override
//   void didRemove(Route<dynamic> route, Route<dynamic>? previousRoute) {
//     log('MyTest didRemove: $route');
//   }

//   @override
//   void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
//     log('MyTest didReplace: $newRoute');
//   }
// }
