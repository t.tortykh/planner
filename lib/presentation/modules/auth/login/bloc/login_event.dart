part of 'login_bloc.dart';

@immutable
abstract class LoginEvent {}

class UserLoginEvent extends LoginEvent {
  final LoginModel user;

  UserLoginEvent({required this.user});
}

class UserLogoutEvent extends LoginEvent {}

class CheckIsLoggedInEvent extends LoginEvent {
  CheckIsLoggedInEvent();
}
