import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:planner/global/model/login_model.dart';
import 'package:planner/global/data/providers/session_data_provider.dart';
import 'package:planner/global/model/user.dart';
import 'package:planner/global/utils/snack_bar.dart';
import 'package:planner/main.dart';
import 'package:planner/presentation/network/authentication_api.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(InitialLoginState()) {
    on<UserLoginEvent>(_onLogin);
    on<UserLogoutEvent>(_onLogout);
    on<CheckIsLoggedInEvent>(_onCheckTokenExists);

    add(CheckIsLoggedInEvent());
  }

  final api = PlannerAuthenticationApi();
  final sessionProvider = SessionDataProvider();

  _onLogin(UserLoginEvent event, Emitter<LoginState> emit) async {
    var response = await api.signIn(event.user);
    if (response != null) {
      if (response['success']) {
        String token = response['token'];
        String name = response['user']['first_name'];
        User user = User.fromJson(response['user']);
        await sessionProvider.setToken(token);
        await sessionProvider.setUserName(name);
        await sessionProvider.setUser(user);
        emit(UserLoggedIn(token: token, userName: name));
        snackBarKey.currentState?.showSnackBar(plannerSnackBar(
          snackBarType: SnackBarType.success,
          message: 'Welcome, $name',
        ));
      } else if (!response['success'] &&
          response['error'] == 'validation_error') {
        emit(FailedLoginForm(invalidMsgFields: response['errors']));
      }
    }
  }

  _onLogout(UserLogoutEvent event, Emitter<LoginState> emit) async {
    await api.logout();
    await sessionProvider.setToken(null);
    await sessionProvider.setUserName(null);
    await sessionProvider.setUser(null);
    emit(InitialLoginState());
  }

  _onCheckTokenExists(
      CheckIsLoggedInEvent event, Emitter<LoginState> emit) async {
    var token = await sessionProvider.getToken().then((value) => value);
    var userName = await sessionProvider.getUserName().then((value) => value);
    User? user = await sessionProvider.getUser();

    if (token?.isNotEmpty ?? false) {
      emit(UserLoggedIn(userName: userName, token: token));
    }
  }
}
