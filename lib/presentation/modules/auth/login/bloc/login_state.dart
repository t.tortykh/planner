part of 'login_bloc.dart';

abstract class LoginState extends Equatable {
  const LoginState();
  @override
  List<Object?> get props => [token, invalidMsgFields != null];

  String? get token => null;
  String? get userName => null;
  Map<String, dynamic>? get invalidMsgFields => null;
}

final class InitialLoginState extends LoginState {}

final class UserLoggedIn extends LoginState {
  @override
  final String? token;
  @override
  final String? userName;
  const UserLoggedIn({required this.token, required this.userName});
}

final class FailedLoginForm extends LoginState {
  @override
  final Map<String, dynamic>? invalidMsgFields;
  const FailedLoginForm({required this.invalidMsgFields});
}
