import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/global/model/login_model.dart';
import 'package:planner/global/utils/validation_message.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/custom_text_field.dart';
import 'package:planner/presentation/widgets/login_headder_icon.dart';
import 'package:planner/global/utils/use_validation.dart';
import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with UseValidation {
  final _formKey = GlobalKey<FormState>();
  late LoginModel user;

  @override
  void initState() {
    // user = LoginModel().loginUser(email: 'ttt@ttt', password: '11111111');
    user = LoginModel().loginUser();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final loginBloc = context.read<LoginBloc>();
    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        if (state is UserLoggedIn) {
          Future.delayed(Duration.zero, () {
            context.pop();
          });
        }
        return Scaffold(
          appBar: AppBar(),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      const LoginHeadder(title: 'Log In'),
                      CustomTextField(
                        key: UniqueKey(),
                        lable: 'Email',
                        value: user.email,
                        validateMode: AutovalidateMode.onUserInteraction,
                        valueChanged: (String value) {
                          user.email = value;
                        },
                        validator: (value) {
                          return requiredField(value);
                          // return validateEmail(value);
                        },
                        validationMessage: loginBloc.state.invalidMsgFields,
                        inputType: InputType.email.title,
                      ),
                      const SizedBox(height: 10),
                      CustomTextField(
                        key: UniqueKey(),
                        lable: 'Password',
                        value: user.password,
                        obscure: true,
                        valueChanged: (String value) {
                          user.password = value;
                        },
                        validateMode: AutovalidateMode.onUserInteraction,
                        validator: (value) {
                          return requiredField(value);
                          // return validatePassword(value);
                        },
                        validationMessage: loginBloc.state.invalidMsgFields,
                        inputType: InputType.password.title,
                      ),
                      const SizedBox(height: 100),
                      ElevatedButton(
                        // style: const ButtonStyle(),
                        onPressed: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            _formKey.currentState?.save();
                            loginBloc.add(UserLoginEvent(user: user));
                          }
                        },
                        child: const Text('Login'),
                      ),
                      const SizedBox(height: 40),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(' Not a member?'),
                          TextButton(
                            onPressed: () => context.go('/login/registration'),
                            child: const Text('Register now'),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
