import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:planner/presentation/widgets/auth/code_field.dart';
import 'package:planner/presentation/modules/auth/register/bloc/registration_bloc.dart';

class EmailVeryficationScreen extends StatelessWidget {
  const EmailVeryficationScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<RegistrationBloc>();
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Code sent to: ${bloc.state.email}'),
                TextButton(
                  onPressed: () {
                    bloc.add(ChangeEmail());
                  },
                  child: const Text('Change email'),
                ),
              ],
            ),
            CodeFieldWidget(
              code: bloc.state.code,
              callback: (newCode) {
                bloc.add(PasteCode(code: newCode));
              },
              collectCode: ({required int index, required String value}) {
                var newCode = bloc.state.code;
                newCode[index] = value;
                bloc.add(InputCode(code: newCode, email: bloc.state.email));
              },
            ),
            TextButton(
              onPressed: () {
                bloc.add(CleanCode());
              },
              child: const Text('Clean'),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                bloc.add(VeryfyEmail());
              },
              child: const Text('Veryfy'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text('Didn\'t get the code?'),
                TextButton(
                  onPressed: () {
                    bloc.add(ResendCode());
                  },
                  child: const Text('Send again'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
