import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/global/model/user_sign_up_request.dart';
import 'package:planner/global/utils/validation_message.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/custom_text_field.dart';
import 'package:planner/presentation/widgets/login_headder_icon.dart';
import 'package:planner/global/utils/use_validation.dart';
import 'package:planner/presentation/modules/auth/register/bloc/registration_bloc.dart';
import 'package:planner/presentation/modules/auth/register/view/email_veryfication_screen.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> with UseValidation {
  final _formKeyReg = GlobalKey<FormState>();
  late UserSignUpRequest user;
  late Widget submitButton;

  @override
  void initState() {
    submitButton = const Text('Next');
    user = UserSignUpRequest().newUser();
    //   user = UserSignUpRequest().newUser(
    // firstName: 'test', lastName: 'test', email: 'test@test.te', password: '123QWEqwe!');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<RegistrationBloc>();

    return BlocBuilder<RegistrationBloc, RegistrationState>(
      builder: (context, state) {
        if (state is SendingCode) {
          submitButton = const CircularProgressIndicator();
        } else {
          submitButton = const Text('Next');
        }
        if (state is EmailVerified) {
          Future.delayed(Duration.zero, () {
            context.pop();
          });
        }
        if (state is SuccsessCodeSent) {
          return const EmailVeryficationScreen();
        }
        return Scaffold(
          appBar: AppBar(),
          body: Stack(
            children: [
              SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: SingleChildScrollView(
                    child: Form(
                      key: _formKeyReg,
                      child: Column(
                        children: [
                          const LoginHeadder(title: 'New Accaunt'),
                          CustomTextField(
                            key: UniqueKey(),
                            lable: 'First Name',
                            value: user.firstName,
                            textInputAction: TextInputAction.next,
                            validateMode: AutovalidateMode.onUserInteraction,
                            valueChanged: (String value) {
                              user.firstName = value;
                            },
                            validator: (value) {
                              return requiredField(value);
                            },
                            inputType: InputType.firstName.title,
                            validationMessage: bloc.state.invalidMsgFields,
                          ),
                          const SizedBox(height: 10),
                          CustomTextField(
                            key: UniqueKey(),
                            lable: 'Last Name',
                            value: user.lastName,
                            textInputAction: TextInputAction.next,
                            validateMode: AutovalidateMode.onUserInteraction,
                            valueChanged: (String value) {
                              user.lastName = value;
                            },
                            validator: (value) {
                              return requiredField(value);
                            },
                            inputType: InputType.lastName.title,
                            validationMessage: bloc.state.invalidMsgFields,
                          ),
                          const SizedBox(height: 10),
                          CustomTextField(
                            key: UniqueKey(),
                            lable: 'Email',
                            value: user.email,
                            textInputAction: TextInputAction.next,
                            validateMode: AutovalidateMode.onUserInteraction,
                            valueChanged: (String value) {
                              user.email = value;
                            },
                            validator: (value) {
                              return validateEmail(value);
                            },
                            validationMessage: bloc.state.invalidMsgFields,
                            inputType: InputType.email.title,
                          ),
                          const SizedBox(height: 20),
                          CustomTextField(
                            key: UniqueKey(),
                            lable: 'Password',
                            value: user.password,
                            obscure: true,
                            textInputAction: TextInputAction.next,
                            validateMode: AutovalidateMode.onUserInteraction,
                            valueChanged: (String value) {
                              user.password = value;
                            },
                            validator: (value) {
                              return validatePassword(value);
                            },
                            inputType: InputType.password.title,
                            validationMessage: bloc.state.invalidMsgFields,
                          ),
                          const SizedBox(height: 20),
                          CustomTextField(
                            lable: 'Confirm Password',
                            value: user.password,
                            obscure: true,
                            textInputAction: TextInputAction.done,
                            validateMode: AutovalidateMode.onUserInteraction,
                            valueChanged: (String value) {},
                            validator: (value) {
                              return matchPasswords(password: user.password, value: value);
                            },
                          ),
                          const SizedBox(height: 20),
                          ElevatedButton(
                            style: const ButtonStyle(),
                            onPressed: () {
                              if (_formKeyReg.currentState?.validate() ?? false) {
                                _formKeyReg.currentState?.save();
                                bloc.add(SubmitForm(user: user));
                              }
                            },
                            child: submitButton,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              //! change to snackbar
              // InformWidget(),
            ],
          ),
        );
      },
    );
  }
}
