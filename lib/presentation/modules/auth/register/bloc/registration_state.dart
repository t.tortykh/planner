part of 'registration_bloc.dart';

abstract class RegistrationState extends Equatable {
  const RegistrationState();

  @override
  List<Object> get props => [email, code, invalidMsgFields != null];
  String get email => '';
  List get code => [null, null, null, null];

  Map<String, dynamic>? get invalidMsgFields => null;
}

final class RegistrationInitial extends RegistrationState {}

final class SendingCode extends RegistrationState {}

class SuccsessCodeSent extends RegistrationState {
  @override
  final String email;
  @override
  final List code;

  const SuccsessCodeSent({
    required this.email,
    required this.code,
  });
//do not need
  SuccsessCodeSent copyWith({required String email, required List? code}) {
    return SuccsessCodeSent(
      email: email,
      code: code ?? this.code,
    );
  }
}

final class FailedRegistrForm extends RegistrationState {
  @override
  final Map<String, dynamic>? invalidMsgFields;
  const FailedRegistrForm({required this.invalidMsgFields});
}

final class EmailVerified extends RegistrationState {}

final class FailedCodeSent extends RegistrationState {
  //todo show message in snackbar
  // flutter: {success: false, error: wrong_code, message: The code is invalid.}
}
