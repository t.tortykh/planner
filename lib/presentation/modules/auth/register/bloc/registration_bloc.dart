import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:planner/global/model/user_sign_up_request.dart';
import 'package:planner/global/utils/snack_bar.dart';
import 'package:planner/main.dart';
import 'package:planner/presentation/network/authentication_api.dart';

part 'registration_event.dart';
part 'registration_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  RegistrationBloc() : super(RegistrationInitial()) {
    on<SubmitForm>(_onSubmitForm);
    on<VeryfyEmail>(_onVeryfyEmail);
    on<ChangeEmail>(_onChangeEmail);
    on<ResendCode>(_onResendCode);
    on<PasteCode>(_onPasteCode);
    on<InputCode>(_onInputCode);
    on<CleanCode>(_onCleanCode);
  }
  final api = PlannerAuthenticationApi();

  _onSubmitForm(SubmitForm event, Emitter<RegistrationState> emit) async {
    emit(SendingCode());

    var resp = await api.registerUser(event.user);
    if (resp['success']) {
      if (event.user.email != null && event.user.email!.isNotEmpty) {
        emit(SuccsessCodeSent(email: event.user.email!, code: state.code));
        snackBarKey.currentState?.showSnackBar(plannerSnackBar(
          snackBarType: SnackBarType.info,
          message: 'Please verify your email address to continue',
        ));
      }
    } else if (!resp['success'] || resp['error'] == 'validation_error') {
      emit(FailedRegistrForm(invalidMsgFields: resp['errors']));
      if (resp['message'] != null) {
        snackBarKey.currentState?.showSnackBar(plannerSnackBar(
          snackBarType: SnackBarType.error,
          message: resp['message'],
        ));
      }
    }
  }

  _onVeryfyEmail(VeryfyEmail event, Emitter<RegistrationState> emit) async {
    String code = state.code.join();
    var resp = await api.requestVerifyEmailCode(email: state.email, code: code);
    if (resp['success']) {
      emit(EmailVerified());
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'Thanks for confirming that information.',
      ));
    }
  }

  _onChangeEmail(ChangeEmail event, Emitter<RegistrationState> emit) {
    emit(RegistrationInitial());
  }

  _onResendCode(ResendCode event, Emitter<RegistrationState> emit) async {
    var resp = await api.sendCodeAgain(email: state.email);
    if (resp['success']) {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.info,
        message: 'We send new code on your email address. Please verify',
      ));
    }
  }

  _onPasteCode(PasteCode event, Emitter<RegistrationState> emit) async {
    emit(SuccsessCodeSent(email: state.email, code: event.code));
  }

  _onInputCode(InputCode event, Emitter<RegistrationState> emit) async {
    emit(SuccsessCodeSent(email: state.email, code: event.code));
  }

  _onCleanCode(CleanCode event, Emitter<RegistrationState> emit) async {
    var newCode = ['', '', '', ''];
    emit(SuccsessCodeSent(email: state.email, code: newCode));
  }
}
