part of 'registration_bloc.dart';

sealed class RegistrationEvent extends Equatable {
  const RegistrationEvent();

  @override
  List<Object> get props => [];
}

class SubmitForm extends RegistrationEvent {
  final UserSignUpRequest user;
  final List? code;
  const SubmitForm({required this.user, this.code});
}

class VeryfyEmail extends RegistrationEvent {}

class ChangeEmail extends RegistrationEvent {}

class ResendCode extends RegistrationEvent {}

class PasteCode extends RegistrationEvent {
  final List code;
  const PasteCode({required this.code});
}

class InputCode extends RegistrationEvent {
  final List code;
  final String email;
  const InputCode({
    required this.code,
    required this.email,
  });
}

class CleanCode extends RegistrationEvent {}
