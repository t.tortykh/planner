import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:planner/global/model/event_model.dart';
import 'package:planner/presentation/modules/calendar/bloc/calendar_bloc.dart';
import 'package:planner/presentation/widgets/date_in_square.dart';
import 'package:planner/presentation/modules/calendar/view/events_card.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarWidget extends StatefulWidget {
  const CalendarWidget({
    super.key,
    required this.onChangePeriod,
    required this.setSelectedDay,
  });
  final Function(String) onChangePeriod;
  final Function(DateTime) setSelectedDay;

  @override
  State<CalendarWidget> createState() => _CalendarWidgetState();
}

class _CalendarWidgetState extends State<CalendarWidget> {
  List<CalendarEventModel>? selectedEvents;
  // CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime _selectedDay = DateTime.now();

  @override
  void initState() {
    super.initState();
  }

  List<CalendarEventModel> _getEventsFromDate(DateTime date) {
    DateTime startDate = DateTime(date.year, date.month, date.day);
    DateTime endDate = startDate.add(const Duration(days: 1));
    final calendarBloc = context.read<CalendarBloc>();

    return calendarBloc.state.eventsList.where((event) {
      DateTime eventStartDate = DateTime.parse(event.dateStart!);
      DateTime eventEndDate = DateTime.parse(event.dateEnd!);

      return (eventStartDate.isBefore(endDate) &&
              eventEndDate.isAfter(startDate)) ||
          (eventStartDate.isAtSameMomentAs(startDate) ||
              eventEndDate.isAtSameMomentAs(endDate));
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    final firstDay = DateTime.utc(2010, 03, 1);
    final lastDay = DateTime.utc(2050, 03, 31);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TableCalendar(
          currentDay: DateTime.now(),
          daysOfWeekHeight: 35,
          daysOfWeekStyle: const DaysOfWeekStyle(
            weekendStyle: TextStyle(
              fontSize: 17,
              color: Color.fromARGB(255, 236, 194, 182),
            ),
            weekdayStyle: TextStyle(
              fontSize: 17,
              color: Color.fromARGB(255, 236, 133, 102),
            ),
            decoration: BoxDecoration(
                // color: Color.fromARGB(255, 236, 133, 102),
                ),
          ),
          headerStyle: const HeaderStyle(
            titleTextStyle: TextStyle(
              color: Colors.white,
              fontSize: 23,
              fontWeight: FontWeight.bold,
            ),
            decoration: BoxDecoration(),
          ),
          startingDayOfWeek: StartingDayOfWeek.monday,
          firstDay: firstDay,
          lastDay: lastDay,
          focusedDay: _focusedDay,
          // calendarFormat: _calendarFormat,
          availableCalendarFormats: const {CalendarFormat.month: 'Month'},
          selectedDayPredicate: (day) {
            return isSameDay(_selectedDay, day);
          },
          onDaySelected: (selectedDay, focusedDay) {
            if (!isSameDay(_selectedDay, selectedDay)) {
              setState(() {
                _selectedDay = selectedDay;
                _focusedDay = focusedDay;
                widget.setSelectedDay(_selectedDay);
              });
            }
          },
          // onFormatChanged: (format) {
          //   print(format);
          //   if (_calendarFormat != format) {
          //     setState(() {
          //       _calendarFormat = format;
          //     });
          //   }
          // },
          onPageChanged: (focusedDay) {
            var period = DateFormat('y-MM').format(focusedDay).toString();
            widget.onChangePeriod(period);
            setState(() {
              _focusedDay = focusedDay;
            });
          },
          eventLoader: _getEventsFromDate,
          calendarStyle: const CalendarStyle(
            markersAlignment: Alignment.bottomRight,
          ),
          calendarBuilders: CalendarBuilders(
            markerBuilder: (context, day, events) => events.isNotEmpty
                ? Container(
                    width: 20,
                    height: 20,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      color: Color.fromARGB(255, 125, 152, 165),
                    ),
                    child: Text(
                      '${events.length}',
                      style: const TextStyle(color: Colors.white),
                    ),
                  )
                : null,
          ),
        ),
        if (_getEventsFromDate(_selectedDay).isEmpty)
          const SizedBox()
        else
          DateInSquare(selectedDay: _selectedDay),
        Expanded(
          child: EventsCardSList(
            selectedDay: _selectedDay,
            getEvents: _getEventsFromDate,
          ),
        ),
      ],
    );
  }
}
