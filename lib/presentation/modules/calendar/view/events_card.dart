import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:planner/global/model/event_model.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/presentation/modules/event/bloc/event_creation_bloc.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';

class EventsCardSList extends StatefulWidget {
  const EventsCardSList({
    super.key,
    required this.selectedDay,
    required this.getEvents,
  });
  final DateTime selectedDay;
  final List<CalendarEventModel> Function(DateTime) getEvents;

  @override
  State<EventsCardSList> createState() => _EventsCardSListState();
}

class _EventsCardSListState extends State<EventsCardSList> {
  Widget _timeText({required CalendarEventModel event}) {
    return DateFormat('dd/MM').format(DateTime.parse(event.dateStart!)) !=
            DateFormat('dd/MM').format(DateTime.parse(event.dateEnd!))
        ? Row(
            children: [
              Text(
                'end: ',
                style: Theme.of(context).textTheme.bodySmall,
              ),
              Text(
                DateFormat('dd/MM').format(DateTime.parse(event.dateEnd!)).toString(),
                style: Theme.of(context).textTheme.bodySmall,
              ),
            ],
          )
        : const SizedBox();
  }

  @override
  Widget build(BuildContext context) {
    final eventBloc = context.read<EventCreationBloc>();

    final groupState = context.select((GroupsBloc bloc) => bloc.state);
    return ListView(
      shrinkWrap: true,
      children: [
        ...widget.getEvents(widget.selectedDay).map((CalendarEventModel event) {
          return SizedBox(
            height: 85,
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 2),
                    color: Colors.white,
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        blurRadius: 10,
                        offset: const Offset(2, 2),
                      ),
                    ],
                  ),
                  clipBehavior: Clip.hardEdge,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        const SizedBox(width: 10),
                        Expanded(
                          flex: 5,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 7),
                              Text(
                                event.name.toString(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.bodyLarge,
                              ),
                              Text(
                                event.description ?? '',
                                style: Theme.of(context).textTheme.bodySmall,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              )
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              getGroups(
                                  groupsList: groupState.groupsList,
                                  event: event,
                                  context: context),
                              Row(
                                children: [
                                  const Icon(Icons.access_time_rounded, size: 13),
                                  getStartTime(event: event, context: context),
                                ],
                              ),
                              _timeText(event: event),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                      onTap: () {
                        eventBloc.add(UpdateEvent(event: event));
                        context.goNamed(
                          'event_card',
                          // extra: {
                          //   'event': event,
                          // },
                          pathParameters: {'id': event.id.toString()},
                        );
                      }),
                ),
              ],
            ),
          );
        })
      ],
    );
  }
}
