import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:intl/intl.dart';
import 'package:planner/global/model/event_model.dart';
import 'package:planner/global/model/group_model.dart';
import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';
import 'package:planner/presentation/network/api_source.dart';

part 'calendar_event.dart';
part 'calendar_state.dart';

class CalendarBloc extends Bloc<CalendarEvent, CalendarState> {
  final LoginBloc loginBloc;
  late final StreamSubscription loginBlocSubscription;

  CalendarBloc(this.loginBloc) : super(CalendarState()) {
    on<GetCalendarDataEvent>(_onGetCalendarEvents);
    on<SetActiveGroup>(_onSetActiveGroup);
    on<SetActivePeriod>(_onSetActivePeriod);
    on<SelectDayEvent>(_onSelectDay);
    on<CleanCalendarDataEvent>(_onCleanCalendarData);

    loginBlocSubscription = loginBloc.stream.listen((event) {
      if (event.token != null) {
        add(GetCalendarDataEvent());
      } else {
        add(CleanCalendarDataEvent());
      }
    });
  }

  @override
  Future<void> close() {
    loginBlocSubscription.cancel();
    return super.close();
  }

  final api = ApiSource();

  _onGetCalendarEvents(GetCalendarDataEvent event, Emitter<CalendarState> emit) async {
    // if (event.token != null) {
    var eventsData = await api.getEvents(period: state.activePeriod, group: state.activeGroup);
    List<CalendarEventModel> eventsList = [];
    if (eventsData['success']) {
      eventsData['events']
          .forEach((element) => eventsList.add(CalendarEventModel.fromJson(element)));
      emit(state.copyWith(events: eventsList));
    }
    // }
  }

  _onSetActiveGroup(SetActiveGroup event, Emitter<CalendarState> emit) {
    emit(state.copyWith(activeGroup: event.groupId));
    add(GetCalendarDataEvent());
  }

  _onSetActivePeriod(SetActivePeriod event, Emitter<CalendarState> emit) {
    emit(state.copyWith(activePeriod: event.period));
    add(GetCalendarDataEvent());
  }

  _onSelectDay(SelectDayEvent event, Emitter<CalendarState> emit) {
    emit(state.copyWith(selectedDay: event.selectedDay));
  }

  _onCleanCalendarData(CleanCalendarDataEvent event, Emitter<CalendarState> emit) {
    emit(state.copyWith(groups: [], events: []));
  }
}
