part of 'calendar_bloc.dart';

class CalendarState {
  final List<CalendarEventModel> eventsList;
  // final List<GroupModel> groupsList;
  final int activeGroup;
  final String activePeriod;
  final DateTime selectedDay;

  // static const int? _defaultActiveGroup = null;
  static final String _defaultActivePeriod = DateFormat('y-MM').format(DateTime.now()).toString();
  static final now = DateTime.now();

  CalendarState({
    this.activeGroup = 0,
    String? activePeriod,
    DateTime? selectedDay,
    this.eventsList = const [],
    // this.groupsList = const [],
  })  : activePeriod = activePeriod ?? _defaultActivePeriod,
        selectedDay = selectedDay ?? now;

  CalendarState copyWith(
      {List<CalendarEventModel>? events,
      List<GroupModel>? groups,
      int? activeGroup,
      String? activePeriod,
      DateTime? selectedDay}) {
    return CalendarState(
        eventsList: events ?? eventsList,
        selectedDay: selectedDay ?? this.selectedDay,
        // groupsList: groups ?? groupsList,
        activeGroup: activeGroup ?? this.activeGroup,
        activePeriod: activePeriod ?? this.activePeriod);
  }
}
