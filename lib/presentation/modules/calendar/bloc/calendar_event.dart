part of 'calendar_bloc.dart';

abstract class CalendarEvent {}

class GetCalendarGroupsEvent extends CalendarEvent {}

class GetCalendarDataEvent extends CalendarEvent {}

class CleanCalendarDataEvent extends CalendarEvent {}

class SetActiveGroup extends CalendarEvent {
  final int? groupId;

  SetActiveGroup({required this.groupId});
}

class SetActivePeriod extends CalendarEvent {
  final String? period;

  SetActivePeriod({required this.period});
}

class SelectDayEvent extends CalendarEvent {
  final DateTime selectedDay;

  SelectDayEvent({required this.selectedDay});
}
