import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const Text('Oops, something go wrong. Please,try again later'),
          TextButton(
            onPressed: () => context.go('/'),
            child: const Text('Main screen'),
          )
        ],
      ),
    );
  }
}
