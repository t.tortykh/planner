import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/global/model/event_model.dart';
import 'package:planner/presentation/modules/event/bloc/event_creation_bloc.dart';
import 'package:planner/presentation/widgets/app_drawer.dart';
import 'package:planner/presentation/widgets/app_floating_button.dart';
import 'package:planner/presentation/widgets/avatar_widget.dart';
import 'package:planner/presentation/widgets/group_list.dart';
import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';
import 'package:planner/presentation/modules/calendar/bloc/calendar_bloc.dart';
import 'package:planner/presentation/modules/calendar/view/calendar_widget.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({super.key});

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    final loginState = context.select((LoginBloc bloc) => bloc.state);
    final calendarBloc = context.read<CalendarBloc>();
    final eventBloc = context.read<EventCreationBloc>();

    final calendarState = context.select((CalendarBloc bloc) => bloc.state);
    final groupState = context.select((GroupsBloc bloc) => bloc.state);

    return Scaffold(
      drawer: loginState.token != null ? const AppDrawer() : null,
      appBar: AppBar(
        leading: loginState.token != null
            ? Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                    icon: Stack(
                      children: [
                        const Icon(Icons.menu),
                        groupState.invitationsList.isNotEmpty
                            ? Positioned(
                                right: 0,
                                child: Container(
                                  width: 10,
                                  height: 10,
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.red,
                                  ),
                                ),
                              )
                            : const SizedBox.shrink(),
                      ],
                    ),
                  );
                },
              )
            : null,
        title: const Text('Planner'),
        actions: [
          loginState.token != null
              ? const AvatarWidget()
              : IconButton(
                  onPressed: () => context.go('/login'),
                  icon: const Icon(Icons.login),
                ),
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            groupState.groupListWithAll.isNotEmpty
                ? GroupListWidget(
                    action: (id) {
                      calendarBloc.add(SetActiveGroup(groupId: id));
                    },
                    groups: groupState.groupListWithAll,
                    nextPaginationPage: groupState.paginationPageGroupList,
                  )
                : const SizedBox(),
            const SizedBox(height: 10),
            Expanded(
              child: Center(
                child: CalendarWidget(
                  onChangePeriod: (period) {
                    loginState.token != null
                        ? calendarBloc.add(SetActivePeriod(period: period))
                        : null;
                  },
                  setSelectedDay: (selectDay) {
                    loginState.token != null
                        ? calendarBloc
                            .add(SelectDayEvent(selectedDay: selectDay))
                        : null;
                  },
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: loginState.token != null
          ? AppFloatingButton(
              buttonName: 'New event',
              onPressed: () {
                eventBloc
                    .add(UpdateEvent(event: CalendarEventModel().newEvent()));
                context.goNamed(
                  'new_event',
                  extra: {
                    'selectedDay': calendarState.selectedDay,
                  },
                );
              })
          : null,
    );
  }
}

///

// class _PullRefreshPageState extends State<PullRefreshPage> {
//   late Future<List<String>> futureNumbersList;

//   @override
//   void initState() {
//     super.initState();
//     futureNumbersList = NumberGenerator().slowNumbers();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: FutureBuilder<List<String>>(
//         future: futureNumbersList,
//         builder: (context, snapshot) {
//           return RefreshIndicator(
//             child: _listView(snapshot),
//             onRefresh: _pullRefresh,
//           );
//         },
//       ),
//     );
//   }

//   Widget _listView(AsyncSnapshot snapshot) {
//     if (snapshot.hasData) {
//       return ListView.builder(
//         itemCount: snapshot.data.length,
//         itemBuilder: (context, index) {
//           return ListTile(
//             title: Text(snapshot.data[index]),
//           );
//         },);
//     }
//     else {
//       return Center(
//         child: Text('Loading data...'),
//       );
//     }
//   }

//   Future<void> _pullRefresh() async {
//     List<String> freshNumbers = await NumberGenerator().slowNumbers();
//     setState(() {
//       futureNumbersList = Future.value(freshNumbers);
//     });
//   }
// }