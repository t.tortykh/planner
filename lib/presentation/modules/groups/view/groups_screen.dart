import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/presentation/modules/groups/components/groups_list.dart';
import 'package:planner/presentation/widgets/app_floating_button.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';
import 'package:planner/presentation/widgets/dialogs/app_bottom_sheet.dart';
import 'group_settings_modal.dart';

enum GroupsType { groupsList, invitationsList, abandonedList }

class GroupsScreen extends StatefulWidget {
  const GroupsScreen({super.key});
  @override
  State<GroupsScreen> createState() => _GroupsScreenState();
}

class _GroupsScreenState extends State<GroupsScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<GroupsBloc>();
    return BlocBuilder<GroupsBloc, GroupsState>(
      builder: (context, state) {
        return Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            title: const Text('Goups List'),
            actions: [
              IconButton(
                onPressed: () => bloc.add(GetGroupsEvent()),
                icon: const Icon(Icons.refresh_sharp),
              )
            ],
            leading: IconButton(
              onPressed: () => context.replace('/'),
              icon: const Icon(Icons.home_outlined),
            ),
          ),
          body: SafeArea(
            child: Column(
              children: [
                Container(
                  height: 45,
                  decoration: BoxDecoration(
                    color: Colors.grey[300],
                    borderRadius: BorderRadius.circular(
                      10.0,
                    ),
                  ),
                  child: TabBar(
                    indicatorSize: TabBarIndicatorSize.tab,
                    indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        10.0,
                      ),
                      color: Colors.pink[300],
                    ),
                    labelColor: Colors.white,
                    unselectedLabelColor: Colors.black,
                    controller: _tabController,
                    tabs: [
                      const FittedBox(child: Tab(text: 'Groups')),
                      Tab(
                        child: FittedBox(
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const Text('Invitations'),
                              const SizedBox(width: 5),
                              state.invitationsList.isNotEmpty
                                  ? Container(
                                      width: 20,
                                      height: 20,
                                      decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.red,
                                      ),
                                      child: Center(
                                        child: Text(
                                          '+${state.invitationsList.length.toString()}',
                                          style: const TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    )
                                  : const SizedBox.shrink(),
                            ],
                          ),
                        ),
                      ),
                      const FittedBox(child: Tab(text: 'Abandonded')),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: const [
                      GroupsList(groupsType: GroupsType.groupsList),
                      GroupsList(groupsType: GroupsType.invitationsList),
                      GroupsList(groupsType: GroupsType.abandonedList),
                    ],
                  ),
                ),
              ],
            ),
          ),
          floatingActionButton: AppFloatingButton(
            buttonName: 'Add new group',
            onPressed: () {
              plannerModalBottomSheet(
                context: context,
                child: const GroupSettingsModal(),
              );
            },
          ),
        );
      },
    );
  }
}
