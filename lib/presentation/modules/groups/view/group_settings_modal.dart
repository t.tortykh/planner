import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/presentation/widgets/dialogs/alert_dialog.dart';
import 'package:planner/global/model/group_model.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/color_picker.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/custom_text_field.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';

class GroupSettingsModal extends StatefulWidget {
  const GroupSettingsModal({
    super.key,
    this.group,
  });
  final GroupModel? group;

  @override
  State<GroupSettingsModal> createState() => _GroupSettingsModalState();
}

class _GroupSettingsModalState extends State<GroupSettingsModal> {
  String? colorGroupString;
  final _formKey = GlobalKey<FormState>();
  late GroupModel group;

  @override
  void initState() {
    if (widget.group != null) {
      group = widget.group!;
    } else {
      group = GroupModel().newGroup();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<GroupsBloc>(context);

    return SafeArea(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              group.id != null
                  ? TextButton(
                      onPressed: () => context.pop(),
                      child: const Text(
                        'Cancel',
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    )
                  : const SizedBox(),
              Text(group.id != null ? 'Edit the group' : 'Add a new group'),
              group.id != null
                  ? TextButton(
                      onPressed: () {
                        showAlertDialog(
                          context,
                          title: 'Abandon the group?',
                          content: 'Are you sure you want to leave the group?',
                          actionButtonName: 'Abandon',
                          action: () {
                            bloc.add(LeaveGroupEvent(groupId: group.id!));
                            context.goNamed('groups');
                          },
                        );
                      },
                      child: const Text(
                        'Abandon',
                        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    )
                  : const SizedBox(),
            ],
          ),
          Form(
            key: _formKey,
            child: Column(
              children: [
                CustomTextField(
                  value: group.name,
                  lable: 'Group name',
                  textInputAction: TextInputAction.next,
                  valueChanged: (String value) {
                    if (value.isNotEmpty) {
                      group.name = value;
                    }
                  },
                  validator: (value) {
                    if (value?.isEmpty ?? true) {
                      return 'Please enter a group name';
                    }
                    return null;
                  },
                ),
                CustomTextField(
                  value: group.description,
                  lable: 'Group description',
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.multiline,
                  maxLines: 5,
                  valueChanged: (String value) {
                    if (value.isNotEmpty) {
                      group.description = value;
                    }
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 30),
          ColorPicker(
            setColor: group.color != null ? parseColor(group.color!) : Colors.green,
            getColor: (Color color) {
              colorGroupString = color.toString();
              group.color = colorGroupString;
            },
          ),
          const SizedBox(height: 70),
          ElevatedButton(
            onPressed: () {
              if (_formKey.currentState?.validate() ?? false) {
                _formKey.currentState?.save();
                if (group.id != null) {
                  bloc.add(EditGroupEvent(group: group));
                } else {
                  bloc.add(
                    AddGroupEvent(
                      groupName: group.name!,
                      description: group.description ?? '',
                      color: group.color ?? 'Color(0xff4caf50)',
                    ),
                  );
                }
                Navigator.pop(context);
              }
            },
            child: Text(group.id != null ? 'Edit group' : 'Add group'),
          ),
        ],
      ),
    );
  }
}
