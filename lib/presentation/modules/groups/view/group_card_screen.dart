import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/global/data/providers/session_data_provider.dart';
import 'package:planner/global/model/group_model.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';
import 'package:planner/presentation/modules/groups/view/groups_screen.dart';
import 'package:planner/presentation/widgets/components/group_partisipants.dart';
import 'package:planner/presentation/widgets/dialogs/alert_dialog.dart';
import 'package:planner/presentation/widgets/dialogs/app_bottom_sheet.dart';
import 'group_settings_modal.dart';

class GroupCardScreen extends StatefulWidget {
  const GroupCardScreen({
    super.key,
    required this.group,
    required this.groupsType,
  });
  final GroupModel group;
  final GroupsType groupsType;

  @override
  State<GroupCardScreen> createState() => _GroupCardScreenState();
}

class _GroupCardScreenState extends State<GroupCardScreen> {
  SessionDataProvider storage = SessionDataProvider();
  late GroupModel group;

  @override
  void initState() {
    group = widget.group;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String groupName = group.name != null ? group.name! : '';
    final bloc = context.read<GroupsBloc>();

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text('Group Details'),
        actions: [
          group.isOwner! && widget.groupsType == GroupsType.groupsList
              ? IconButton(
                  onPressed: () => plannerModalBottomSheet(
                    context: context,
                    child: GroupSettingsModal(
                      group: widget.group,
                    ),
                  ).then(
                    (value) {
                      setState(() {});
                    },
                  ),
                  icon: const Icon(Icons.settings_suggest_outlined),
                )
              : const SizedBox.shrink(),
          widget.groupsType == GroupsType.abandonedList
              ? IconButton(
                  onPressed: () {
                    showAlertDialog(
                      context,
                      title: 'Return to the group?',
                      content: 'Are you sure you want to return to the group?',
                      actionButtonName: 'Return',
                      action: () {
                        bloc.add(ReturnToGroupEvent(groupId: group.id!));
                        context.goNamed('groups');
                      },
                    );
                  },
                  icon: const Icon(Icons.restore_from_trash_rounded),
                )
              : const SizedBox.shrink(),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 45),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Center(
                        child: Text(
                          groupName,
                          maxLines: 2,
                          textAlign: TextAlign.end,
                          overflow: TextOverflow.clip,
                          style: const TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    CircleAvatar(
                      backgroundColor: group.color != null
                          ? parseColor(group.color!)
                          : Colors.grey,
                      minRadius: 40,
                      child: Text(
                        groupName.characters.first,
                        style: const TextStyle(
                            fontSize: 40, fontWeight: FontWeight.bold),
                      ),
                    ),
                    const SizedBox(width: 10)
                  ],
                ),
                const SizedBox(height: 10),
                Text(
                  'Description:',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const SizedBox(height: 5),
                Text(
                  group.description ?? '',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                const SizedBox(height: 10),
                Text(
                  'Owner:',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const SizedBox(height: 5),
                Text(
                  getOwnerName(group),
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                const SizedBox(height: 20),
                Text(
                  'Participants:',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const SizedBox(height: 7),
                GroupPartisipantsWidget(
                  isExpandable: widget.groupsType == GroupsType.groupsList,
                  group: group,
                  groupsType: widget.groupsType,
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: group.invitationData != null
          ? Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  width: 120,
                  child: FloatingActionButton.extended(
                    backgroundColor: Colors.green,
                    shape: const RoundedRectangleBorder(
                        side: BorderSide(width: 3, color: Colors.white),
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                    onPressed: () {
                      if (group.invitationData?.id != null) {
                        bloc.add(AcceptInvitationEvent(
                            invitationId: group.invitationData!.id));
                        context.pop();
                      }
                    },
                    heroTag: 'accept',
                    label: const Text('Accept'),
                  ),
                ),
                const SizedBox(width: 20),
                SizedBox(
                  width: 120,
                  child: FloatingActionButton.extended(
                    backgroundColor: Colors.red,
                    shape: const RoundedRectangleBorder(
                        side: BorderSide(width: 3, color: Colors.white),
                        borderRadius: BorderRadius.all(Radius.circular(15.0))),
                    onPressed: () {
                      showAlertDialog(
                        context,
                        title: 'Decline this group?',
                        content: 'Are you sure you want to decline this group?',
                        actionButtonName: 'Decline',
                        action: () {
                          if (group.invitationData?.id != null) {
                            context.pop();
                            bloc.add(DeclineInvitationEvent(
                                invitationId: group.invitationData!.id));
                          }
                        },
                      );
                    },
                    heroTag: 'decline',
                    label: const Text('Decline'),
                  ),
                ),
              ],
            )
          : const SizedBox.shrink(),
    );
  }
}
