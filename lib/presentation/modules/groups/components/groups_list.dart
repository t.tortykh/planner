import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/global/model/group_model.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';
import 'package:planner/presentation/modules/groups/view/groups_screen.dart';
import 'package:planner/presentation/widgets/dialogs/alert_dialog.dart';

class GroupsList extends StatefulWidget {
  final GroupsType groupsType;
  const GroupsList({super.key, required this.groupsType});

  @override
  State<GroupsList> createState() => _GroupsListState();
}

class _GroupsListState extends State<GroupsList> {
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_scrollListener);
    _scrollController.dispose();
    super.dispose();
  }

  void _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      BlocProvider.of<GroupsBloc>(context).add(UpdateGroupsEvent());
    }
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<GroupsBloc>(context);
    return BlocBuilder<GroupsBloc, GroupsState>(
      builder: (context, state) {
        List<GroupModel> groups = state.getActiveList(widget.groupsType);
        return groups.isNotEmpty
            ? Column(
                children: [
                  Expanded(
                    child: ListView.builder(
                      controller: _scrollController,
                      physics: const AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: groups.length,
                      itemBuilder: ((context, index) {
                        var colorGroup = groups[index].color;
                        colorGroup != null ? parseColor(colorGroup) : null;
                        if (index > 8 &&
                            index == groups.length - 1 &&
                            state.paginationPageGroupList != null) {
                          return const Center(
                            child: CircularProgressIndicator(strokeWidth: 2),
                          );
                        }
                        return Slidable(
                          key: const ValueKey(0),
                          endActionPane: ActionPane(
                            motion: const ScrollMotion(),
                            children: [
                              getActionForGroupType(
                                  widget.groupsType, bloc, groups, index),
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: SizedBox(
                              height: 80,
                              width: double.infinity,
                              child: Stack(
                                children: [
                                  Container(
                                    decoration: const BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(10)),
                                    ),
                                    clipBehavior: Clip.hardEdge,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 20,
                                          color: colorGroup != null
                                              ? parseColor(colorGroup)
                                              : Colors.grey,
                                        ),
                                        const SizedBox(width: 10),
                                        Expanded(
                                          child: Column(
                                            children: [
                                              const SizedBox(height: 5),
                                              Expanded(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: [
                                                    Text(
                                                      groups[index].name ?? '',
                                                      style: const TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Row(
                                                children: [
                                                  const Icon(
                                                    Icons.people_outline,
                                                    color: Colors.black,
                                                  ),
                                                  Text(groups[index]
                                                          .participants
                                                          ?.length
                                                          .toString() ??
                                                      '0')
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        const SizedBox(width: 15),
                                        CircleAvatar(
                                          backgroundColor: groups[index]
                                                      .color !=
                                                  null
                                              ? parseColor(groups[index].color!)
                                              : Colors.grey,
                                          minRadius: 30,
                                          child: Text(
                                            groups[index]
                                                .name!
                                                .characters
                                                .first,
                                            style: const TextStyle(
                                                fontSize: 25,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        const SizedBox(width: 15),
                                      ],
                                    ),
                                  ),
                                  Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      onTap: () => context.go(
                                        '/groups/group_card',
                                        extra: {
                                          'group': groups[index],
                                          'groupsType': widget.groupsType,
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                    ),
                  ),
                ],
              )
            : const Center(
                child: Icon(
                  Icons.workspaces_outlined,
                  color: Colors.white,
                  size: 40,
                ),
              );
      },
    );
  }

  SlidableAction getActionForGroupType(
      GroupsType type, GroupsBloc bloc, List<GroupModel> groups, int index) {
    switch (type) {
      case GroupsType.groupsList:
        return SlidableAction(
          onPressed: (context) {
            showAlertDialog(
              context,
              title: 'Are you sure you want to leave the group?',
              content: 'All your events from this group will be deleted',
              actionButtonName: 'Abandon',
              action: () {
                bloc.add(LeaveGroupEvent(groupId: groups[index].id!));
              },
            );
          },
          backgroundColor: const Color.fromARGB(255, 166, 46, 46),
          foregroundColor: Colors.white,
          icon: Icons.logout_outlined,
          label: 'Abandon',
        );
      case GroupsType.invitationsList:
        return SlidableAction(
          onPressed: (context) {
            showAlertDialog(
              context,
              title: 'Decline invitation?',
              content:
                  'Are you sure you want to decline this group invitation?',
              actionButtonName: 'Decline',
              action: () {
                if (groups[index].invitationData != null) {
                  bloc.add(DeclineInvitationEvent(
                      invitationId: groups[index].invitationData!.id));
                }
              },
            );
          },
          backgroundColor: const Color.fromARGB(255, 166, 46, 46),
          foregroundColor: Colors.white,
          icon: Icons.not_interested_rounded,
          label: 'Decline',
        );
      case GroupsType.abandonedList:
        return SlidableAction(
          onPressed: (context) {
            showAlertDialog(
              context,
              title: 'Are you sure you want to delete the group?',
              content: 'All your events from this group will be deleted',
              actionButtonName: 'Delete',
              action: () {
                bloc.add(DeleteGroupEvent(groupId: groups[index].id!));
              },
            );
          },
          backgroundColor: const Color.fromARGB(255, 166, 46, 46),
          foregroundColor: Colors.white,
          label: 'Delete group',
          icon: Icons.delete_forever,
        );
    }
  }
}
