import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/global/utils/use_validation.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/custom_text_field.dart';

class NewPartisipantForm extends StatefulWidget {
  final int? groupId;
  const NewPartisipantForm({
    super.key,
    required this.groupId,
    // required this.isLoading,
  });
  // final bool isLoading;

  @override
  State<NewPartisipantForm> createState() => _NewPartisipantFormState();
}

class _NewPartisipantFormState extends State<NewPartisipantForm> with UseValidation {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  String titleShareMore = '';

  getStatus() {
    setState(() {
      _isLoading != _isLoading;
    });
  }

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<GroupsBloc>();
    return BlocBuilder<GroupsBloc, GroupsState>(
      builder: (context, state) {
        if (state is SendingPartisipantEmail) {
          _isLoading = true;
        }
        if (state is FaildSentPartisipantEmail) {
          _isLoading = false;
        }
        if (state is SuccesfulySentPartisipantEmail) {
          titleShareMore = 'Success! Add more partisipants';
          _isLoading = false;
        }

        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                const Expanded(
                  child: Center(
                    child: Text('Add a new partisipant'),
                  ),
                ),
                TextButton(
                  onPressed: () => context.pop(),
                  child: const Text(
                    'Cancel',
                    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                )
              ],
            ),
            _isLoading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : Column(
                    children: [
                      Text(titleShareMore),
                      Form(
                        key: _formKey,
                        child: CustomTextField(
                          lable: 'Enter email address',
                          value: bloc.state.email,
                          validateMode: AutovalidateMode.onUserInteraction,
                          valueChanged: (String value) {
                            bloc.add(ChangingEmailPartisipant(email: value));
                          },
                          validator: (value) {
                            return validateEmail(value);
                          },
                        ),
                      ),
                    ],
                  ),
            const SizedBox(height: 70),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState?.validate() ?? false) {
                  _formKey.currentState?.save();
                  if (widget.groupId != null) {
                    bloc.add(ShareGroupEvent(groupId: widget.groupId!));
                    // context.pop();
                  }
                }
              },
              child: const Text('Add partisipant'),
            ),
          ],
        );
      },
    );
  }
}
