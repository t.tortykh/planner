part of 'groups_bloc.dart';

abstract class GroupsState extends Equatable {
  @override
  List<Object> get props => [
        groupsList,
        groupListWithAll,
        email,
        invitationsList,
        abandonedList,
        paginationPageGroupList != null,
      ];

  List<GroupModel> get groupsList => [];
  List<GroupModel> get groupListWithAll => [];
  String get email => '';
  int? get paginationPageGroupList => null;
  List<GroupModel> get invitationsList => [];
  List<GroupModel> get abandonedList => [];

  dynamic getActiveList(GroupsType type) {
    switch (type) {
      case GroupsType.groupsList:
        return groupsList;
      case GroupsType.invitationsList:
        return invitationsList;
      case GroupsType.abandonedList:
        return abandonedList;
    }
  }
}

class GroupsStateInit extends GroupsState {}

class LoadingGroupsState extends GroupsState {
  // @override
  // final List<GroupModel> oldGroupsList;
  // LoadingGroupsState({required this.oldGroupsList});
}

class LoadedGroupState extends GroupsState {
  @override
  final int? paginationPageGroupList;
  @override
  final List<GroupModel> groupsList;
  @override
  final List<GroupModel> groupListWithAll;
  @override
  final List<GroupModel> invitationsList;
  @override
  final List<GroupModel> abandonedList;

  LoadedGroupState({
    required this.invitationsList,
    required this.abandonedList,
    required this.groupsList,
    required this.groupListWithAll,
    required this.paginationPageGroupList,
  });
}

class UpdatedGroupsState extends GroupsState {
  @override
  final List<GroupModel> groupsList;
  @override
  final List<GroupModel> groupListWithAll;
  @override
  final List<GroupModel> invitationsList;
  @override
  final List<GroupModel> abandonedList;
  @override
  final int? paginationPageGroupList;

  UpdatedGroupsState({
    required this.invitationsList,
    required this.groupsList,
    required this.groupListWithAll,
    required this.abandonedList,
    required this.paginationPageGroupList,
  });
}

class EmailPartisipantAdded extends GroupsState {
  @override
  final String email;

  EmailPartisipantAdded({required this.email});
}

class SendingPartisipantEmail extends GroupsState {
  @override
  final String email;

  SendingPartisipantEmail({required this.email});
}

class SuccesfulySentPartisipantEmail extends GroupsState {
  @override
  final String email;

  SuccesfulySentPartisipantEmail({required this.email});
}

class FaildSentPartisipantEmail extends GroupsState {}
