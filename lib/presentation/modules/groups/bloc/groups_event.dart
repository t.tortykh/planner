part of 'groups_bloc.dart';

abstract class GroupsEvent {}

class GetGroupsEvent extends GroupsEvent {}

class UpdateGroupsEvent extends GroupsEvent {}

class AddGroupEvent extends GroupsEvent {
  final String groupName;
  final String color;
  final String description;

  AddGroupEvent({
    required this.color,
    required this.groupName,
    required this.description,
  });
}

class EditGroupEvent extends GroupsEvent {
  final GroupModel group;

  EditGroupEvent({required this.group});
}

class DeleteGroupEvent extends GroupsEvent {
  final int groupId;

  DeleteGroupEvent({required this.groupId});
}

class CleanGroupsData extends GroupsEvent {}

class ChangingEmailPartisipant extends GroupsEvent {
  final String email;

  ChangingEmailPartisipant({required this.email});
}

class ShareGroupEvent extends GroupsEvent {
  final int groupId;

  ShareGroupEvent({required this.groupId});
}

class AcceptInvitationEvent extends GroupsEvent {
  final int invitationId;

  AcceptInvitationEvent({required this.invitationId});
}

class DeclineInvitationEvent extends GroupsEvent {
  final int invitationId;

  DeclineInvitationEvent({required this.invitationId});
}

class LeaveGroupEvent extends GroupsEvent {
  final int groupId;

  LeaveGroupEvent({required this.groupId});
}

class ReturnToGroupEvent extends GroupsEvent {
  final int groupId;

  ReturnToGroupEvent({required this.groupId});
}
