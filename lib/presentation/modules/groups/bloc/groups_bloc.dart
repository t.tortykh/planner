import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:planner/global/model/group_model.dart';
import 'package:planner/global/model/invitation_data.dart';
import 'package:planner/global/utils/snack_bar.dart';
import 'package:planner/main.dart';
import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';
import 'package:planner/presentation/modules/calendar/bloc/calendar_bloc.dart';
import 'package:planner/presentation/modules/groups/view/groups_screen.dart';
import 'package:planner/presentation/network/api_source.dart';

part 'groups_event.dart';
part 'groups_state.dart';

class GroupsBloc extends Bloc<GroupsEvent, GroupsState> {
  final CalendarBloc calendarBloc;
  late final StreamSubscription<LoginState> loginBlocSubscription;
  GroupsBloc(
    this.calendarBloc,
  ) : super(GroupsStateInit()) {
    //get lists
    on<GetGroupsEvent>(_onGetGroups);
    on<UpdateGroupsEvent>(_onUpdateGroups);
    // work with groups
    on<AddGroupEvent>(_onAddGroup);
    on<EditGroupEvent>(_onEditGroup);
    on<DeleteGroupEvent>(_onDeleteGroup);
    on<LeaveGroupEvent>(_onLeaveGroup);
    on<ReturnToGroupEvent>(_onReturnToGroup);
    // empty data
    on<CleanGroupsData>(_onCleanGroupsData);
    //invitations
    on<ChangingEmailPartisipant>(_onChangingEmail);
    on<ShareGroupEvent>(_onShareGroupEvent);
    on<AcceptInvitationEvent>(_onAcceptInvitation);
    on<DeclineInvitationEvent>(_onDeclineInvitation);

    loginBlocSubscription = calendarBloc.loginBloc.stream.listen(
      (event) {
        if (event.token != null) {
          add(GetGroupsEvent());
        } else {
          add(CleanGroupsData());
        }
      },
    );
  }

  @override
  Future<void> close() {
    loginBlocSubscription.cancel();
    return super.close();
  }

  final api = ApiSource();

  _onGetGroups(GetGroupsEvent event, Emitter<GroupsState> emit) async {
    //get invitations aand abandoned groups
    var response = await api.getAdditional();
    List<GroupModel> invitations = [];
    List<GroupModel> abandonedList = [];
    if (response['success']) {
      if (response['invitations'] != null && response['invitations'] != []) {
        response['invitations'].forEach((element) {
          GroupModel group = GroupModel.fromJson(element['group']);
          group.invitationData = InvitationData.fromJson(element);
          invitations.add(group);
        });
      }
      if (response['abandoned_groups'] != null &&
          response['abandoned_groups'] != []) {
        response['abandoned_groups'].forEach((element) {
          abandonedList.add(GroupModel.fromJson(element));
        });
      }
    }
    //init group
    int? page = 1;
    var resp = await api.getPaginatedGroups(page: page, itemsPerPage: 10);

    List<GroupModel> groupList = [];
    if (resp['success']) {
      resp['groups']['data']
          .forEach((element) => groupList.add(GroupModel.fromJson(element)));
      List<GroupModel> groupListWithAll = [...groupList];
      groupListWithAll.insert(0, GroupModel(name: 'All', id: 0));
      page = resp['groups']['next_page_url'] != null ? page + 1 : null;

      emit(UpdatedGroupsState(
          groupsList: groupList,
          groupListWithAll: groupListWithAll,
          invitationsList: invitations,
          abandonedList: abandonedList,
          paginationPageGroupList: page));
    }
  }

  _onUpdateGroups(UpdateGroupsEvent event, Emitter<GroupsState> emit) async {
    int? page = state.paginationPageGroupList;
    if (page == null) {
      emit(LoadedGroupState(
        groupsList: state.groupsList,
        groupListWithAll: state.groupListWithAll,
        paginationPageGroupList: page,
        abandonedList: state.abandonedList,
        invitationsList: state.invitationsList,
      ));
      return;
    }
    var newGroupList = state.groupsList;

    var resp = await api.getPaginatedGroups(page: page, itemsPerPage: 10);
    List<GroupModel> groupList = [];
    if (resp['success']) {
      resp['groups']['data']
          .forEach((element) => groupList.add(GroupModel.fromJson(element)));
      newGroupList.addAll(groupList);
      List<GroupModel> groupListWithAll = [...newGroupList];
      groupListWithAll.insert(0, GroupModel(name: 'All', id: 0));
      page = resp['groups']['next_page_url'] != null ? page + 1 : null;

      emit(LoadedGroupState(
        groupsList: newGroupList,
        groupListWithAll: groupListWithAll,
        paginationPageGroupList: page,
        abandonedList: state.abandonedList,
        invitationsList: state.invitationsList,
      ));
    }
  }

  _onAddGroup(AddGroupEvent event, Emitter<GroupsState> emit) async {
    var resp =
        await api.addGroup(event.groupName, event.color, event.description);
    if (resp['success']) {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'Your group "${event.groupName}" successfully added!',
      ));
    }
    add(GetGroupsEvent());
  }

  _onEditGroup(EditGroupEvent event, Emitter<GroupsState> emit) async {
    await api.editGroup(event.group.groupToJson());
    add(GetGroupsEvent());
  }

  _onDeleteGroup(DeleteGroupEvent event, Emitter<GroupsState> emit) async {
    var resp = await api.deleteGroup(groupId: event.groupId);
    if (resp['success']) {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'Your group has been deleted',
      ));
    }
    calendarBloc.add(GetCalendarDataEvent());
    add(GetGroupsEvent());
  }

  _onCleanGroupsData(CleanGroupsData event, Emitter<GroupsState> emit) {
    emit(GroupsStateInit());
  }

  _onChangingEmail(ChangingEmailPartisipant event, Emitter<GroupsState> emit) {
    emit(EmailPartisipantAdded(email: event.email));
  }

  _onShareGroupEvent(ShareGroupEvent event, Emitter<GroupsState> emit) async {
    emit(SendingPartisipantEmail(email: state.email));
    var resp = await api.shareGroup(groupId: event.groupId, email: state.email);
    if (resp['success']) {
      emit(SuccesfulySentPartisipantEmail(email: ''));
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: '${state.email} has been invited',
      ));
    } else {
      emit(FaildSentPartisipantEmail());
    }
  }

  _onAcceptInvitation(
      AcceptInvitationEvent event, Emitter<GroupsState> emit) async {
    var resp = await api.handleInvitationGroup(
        invitationId: event.invitationId, accepted: 1);
    if (resp['success']) {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'Group has been accepted',
      ));
    }
    calendarBloc.add(GetCalendarDataEvent());
    add(GetGroupsEvent());
  }

  _onDeclineInvitation(
      DeclineInvitationEvent event, Emitter<GroupsState> emit) async {
    var resp = await api.handleInvitationGroup(
        invitationId: event.invitationId, accepted: 0);
    if (resp['success']) {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.info,
        message: 'Group has been decline',
      ));
    }
    add(GetGroupsEvent());
  }

  _onLeaveGroup(LeaveGroupEvent event, Emitter<GroupsState> emit) async {
    var resp = await api.leaveGroup(groupId: event.groupId);
    if (resp['success']) {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'You abandoned this group',
      ));
    }
    calendarBloc.add(GetCalendarDataEvent());
    add(GetGroupsEvent());
  }

  _onReturnToGroup(ReturnToGroupEvent event, Emitter<GroupsState> emit) async {
    var resp = await api.returnToGroup(groupId: event.groupId);
    if (resp['success']) {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'You abandoned this group',
      ));
    }
    calendarBloc.add(GetCalendarDataEvent());
    add(GetGroupsEvent());
  }
}
