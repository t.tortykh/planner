import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/global/utils/utils.dart';
// import 'package:planner/presentation/modules/auth/login/bloc/login_bloc.dart';
import 'package:planner/presentation/modules/event/bloc/event_creation_bloc.dart';
import 'package:planner/presentation/widgets/app_floating_button.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';
import 'package:planner/presentation/widgets/event_date_column.dart';

class EventCardScreen extends StatefulWidget {
  const EventCardScreen({
    super.key,
  });

  @override
  State<EventCardScreen> createState() => _EventCardScreenState();
}

class _EventCardScreenState extends State<EventCardScreen> {
  @override
  Widget build(BuildContext context) {
    // final loginBloc = context.read<LoginBloc>();
    final groupState = context.select((GroupsBloc bloc) => bloc.state);
    final eventBloc = context.read<EventCreationBloc>();

    Color groupColor = Colors.grey;
    String groupName = '';

    groupColor = getGroupColor(
        groupsList: groupState.groupsList, event: eventBloc.state.event!);
    groupName = getGroupName(
        groupsList: groupState.groupsList, event: eventBloc.state.event!);
    return Scaffold(
      appBar: AppBar(title: const Text('Event Details')),
      body: SafeArea(
        child: Padding(
          padding:
              const EdgeInsets.only(left: 16, right: 16, top: 30, bottom: 45),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Chip(
                      label: Text(
                        groupName,
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                      backgroundColor: groupColor,
                    ),
                    Expanded(
                      child: Center(
                        child: Text(
                          eventBloc.state.event?.name ?? '',
                          maxLines: 2,
                          textAlign: TextAlign.end,
                          overflow: TextOverflow.clip,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.width * 0.66,
                  decoration: BoxDecoration(
                    border: Border.all(
                        color: const Color.fromARGB(255, 61, 61, 61), width: 2),
                    borderRadius: const BorderRadius.all(Radius.circular(15)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.white.withOpacity(0.2),
                        blurRadius: 10,
                      ),
                    ],
                  ),
                  child: Row(
                    children: [
                      //todo change to the img if its uploaded
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Description:',
                                style: Theme.of(context).textTheme.titleMedium,
                              ),
                              const SizedBox(height: 5),
                              Text(
                                eventBloc.state.event?.description ?? '',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ],
                          ),
                        ),
                      ),
                      EventDateColumn(event: eventBloc.state.event!),
                    ],
                  ),
                ),
                const SizedBox(height: 40),

                // Text(
                //   'Partisipants:',
                //   style: Theme.of(context).textTheme.titleMedium,
                // ),
                //todo create separate widget partisipants line
                // const SizedBox(height: 5),
                // Row(
                //   children: [
                //     CircleAvatar(child: Image.asset('lib/global/assets/user.png')),
                //     const SizedBox(width: 10),
                //     Text(
                //       loginBloc.state.userName ?? '',
                //       style: Theme.of(context).textTheme.bodyMedium,
                //     )
                //   ],
                // ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: AppFloatingButton(
        buttonName: 'Edit',
        onPressed: () => context.pushNamed(
          'event_form',
          // extra: {},
          pathParameters: {
            'id': eventBloc.state.event!.id.toString(),
          },
        ).then((value) {
          setState(() {});
        }),
      ),
    );
  }
}
