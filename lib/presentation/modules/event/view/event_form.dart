import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:planner/presentation/modules/groups/view/group_settings_modal.dart';
import 'package:planner/presentation/widgets/dialogs/alert_dialog.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/presentation/widgets/dialogs/app_bottom_sheet.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/custom_date_time_picker.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/custom_text_field.dart';
import 'package:planner/presentation/widgets/reuseable_widgets/drop_down_groups.dart';
import 'package:planner/presentation/modules/event/bloc/event_creation_bloc.dart';
import 'package:planner/presentation/modules/groups/bloc/groups_bloc.dart';

class EventScreenForm extends StatefulWidget {
  const EventScreenForm({
    super.key,
    this.selectedDay,
  });
  final DateTime? selectedDay;

  @override
  State<EventScreenForm> createState() => _EventScreenFormState();
}

class _EventScreenFormState extends State<EventScreenForm> {
  bool onlyDate = false;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    final bloc = context.read<EventCreationBloc>();
    bloc.add(InitEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<EventCreationBloc>(context);
    final eventState = context.select((EventCreationBloc bloc) => bloc.state);
    final groupState = context.select((GroupsBloc bloc) => bloc.state);
    return Scaffold(
      appBar: AppBar(
        title: Text(bloc.state.event?.name ?? 'New Event'),
        actions: [
          bloc.state.event?.id != null
              ? IconButton(
                  onPressed: () async {
                    showAlertDialog(
                      context,
                      title: 'Are you sure you want to delete?',
                      content: 'This events will be deleted',
                      actionButtonName: 'Delete',
                      action: () {
                        bloc.add(DeleteEventForm());

                        context.goNamed('main_screen');
                      },
                    );
                  },
                  icon: const Icon(Icons.delete_outline))
              : const SizedBox()
        ],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  CustomTextField(
                    value: eventState.event?.name ?? '',
                    lable: 'Event name',
                    textInputAction: TextInputAction.next,
                    valueChanged: (String value) {
                      if (value.isNotEmpty) {
                        bloc.add(UpdateNameEvent(newName: value));
                      }
                    },
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return 'Please enter a name';
                      }
                      return null;
                    },
                  ),
                  CustomTextField(
                    value: eventState.event?.description ?? '',
                    lable: 'Event description',
                    textInputAction: TextInputAction.next,
                    valueChanged: (String value) {
                      if (value.isNotEmpty) {
                        bloc.add(UpdateDescriptionEvent(newDescription: value));
                      }
                    },
                  ),
                  CustomDropDownMenu(
                    value: eventState.event?.id != null &&
                            groupState.groupsList.isNotEmpty &&
                            bloc.state.event?.group != null
                        ? groupState.groupsList.firstWhere(
                            (object) => object.id == bloc.state.event?.group)
                        // todo: null change to favorite
                        : null,
                    list: groupState.groupsList,
                    valueChanged: (value) {
                      bloc.add(UpdateGroupEvent(newGroupId: value.id));
                    },
                    validator: (value) {
                      if (value == null) {
                        return 'Please choose the group or add new one';
                      }
                      return null;
                    },
                  ),
                  Center(
                    child: TextButton(
                      onPressed: () {
                        plannerModalBottomSheet(
                          context: context,
                          child: const GroupSettingsModal(),
                        );
                      },
                      child: const Text('Add new group',
                          style: TextStyle(
                            decoration: TextDecoration.underline,
                            color: Colors.grey,
                          )),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'All day',
                        style: TextStyle(
                            fontSize: 16,
                            color: Color.fromARGB(255, 191, 121, 121),
                            fontWeight: FontWeight.bold),
                      ),
                      Switch.adaptive(
                          value: onlyDate,
                          onChanged: (value) {
                            setState(() {
                              onlyDate = !onlyDate;
                            });
                          }),
                    ],
                  ),
                  const FormDivider(),
                  eventState.event?.dateStart != null
                      ? CustomDateTimePicker(
                          onlyDate: onlyDate,
                          lable: 'Starts: ',
                          initDate: parseDate(eventState.event!.dateStart!),
                          valueChanged: (DateTime value) {
                            bloc.add(UpdateStartTimeEvent(newStartTime: value));
                          },
                        )
                      : const SizedBox(),
                  const FormDivider(),
                  eventState.event?.dateEnd != null
                      ? CustomDateTimePicker(
                          onlyDate: onlyDate,
                          lable: 'Ends: ',
                          initDate: parseDate(eventState.event!.dateEnd!),
                          valueChanged: (DateTime value) {
                            bloc.add(UpdateEndTimeEvent(newEndTime: value));
                          },
                        )
                      : const SizedBox(),
                  const FormDivider(),
                  const SizedBox(height: 55),
                  ElevatedButton(
                    // style: const ButtonStyle(),
                    onPressed: () {
                      if (_formKey.currentState?.validate() ?? false) {
                        _formKey.currentState?.save();
                        bloc.add(SubmitEventForm());
                        Navigator.pop(context);

                        // context.pop();
                      }
                    },
                    child: Text(
                        bloc.state.event?.id != null ? 'Edit' : 'Add event'),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
