part of 'event_creation_bloc.dart';

class EventCreationState extends Equatable {
  const EventCreationState({
    this.event,
    this.startDate,
  });

  final CalendarEventModel? event;
  final DateTime? startDate;

  bool get isNewTodo => event == null;

  EventCreationState copyWith({
    CalendarEventModel? event,
    DateTime? startDate,
    bool? isLoading,
  }) {
    return EventCreationState(
      event: event ?? this.event,
      startDate: startDate ?? this.startDate,
    );
  }

  @override
  List<Object?> get props => [
        event,
        startDate,
      ];
}
