import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:planner/global/model/event_model.dart';
import 'package:planner/global/utils/snack_bar.dart';
import 'package:planner/global/utils/utils.dart';
import 'package:planner/main.dart';
import 'package:planner/presentation/modules/calendar/bloc/calendar_bloc.dart';
import 'package:planner/presentation/network/api_source.dart';

part 'event_creation_event.dart';
part 'event_creation_state.dart';

class EventCreationBloc extends Bloc<EventCreationEvent, EventCreationState> {
  final CalendarBloc? calendarBloc;
  DateTime? secectDay;
  EventCreationBloc(
    this.calendarBloc,
  ) : super(const EventCreationState()) {
    on<InitEvent>(_init);
    on<UpdateEvent>(_onUpdateEvent);
    on<SubmitEventForm>(_submitEvent);
    on<DeleteEventForm>(_deletEvent);
    on<UpdateNameEvent>(_onChangeName);
    on<UpdateDescriptionEvent>(_onChangeDescription);
    on<UpdateGroupEvent>(_onChangeGroup);
    on<UpdateStartTimeEvent>(_onChangeStartDate);
    on<UpdateEndTimeEvent>(_onChangeEndDate);
  }

  final api = ApiSource();
  _onUpdateEvent(UpdateEvent event, Emitter<EventCreationState> emit) {
    emit(state.copyWith(event: event.event));
  }

  _init(InitEvent event, Emitter<EventCreationState> emit) async {
    DateTime start;
    DateTime end;

    if (state.event?.id == null) {
      // no event
      if (calendarBloc?.state.selectedDay != null) {
        secectDay = calendarBloc?.state.selectedDay;
        DateTime selectedDayParsed =
            DateTime.parse(secectDay!.toIso8601String());
        TimeOfDay currentTime = TimeOfDay.now();
        int hour = currentTime.hour;
        int minute = currentTime.minute;

        secectDay = DateTime(
          selectedDayParsed.year,
          selectedDayParsed.month,
          selectedDayParsed.day,
          hour,
          minute,
        );
      }
      //with selected date or no

      start = secectDay ?? DateTime.now();
      end = secectDay?.add(const Duration(hours: 1)) ??
          DateTime.now().add(const Duration(hours: 1));
    } else {
      start = parseDate(state.event!.dateStart!);
      end = parseDate(state.event!.dateEnd!);
    }
    add(UpdateStartTimeEvent(newStartTime: start));
    add(UpdateEndTimeEvent(newEndTime: end));
  }

  _submitEvent(SubmitEventForm event, Emitter<EventCreationState> emit) async {
    dynamic response;
    if (state.event?.id == null) {
      response = await api.submitEvent(state.event?.newEventToJson());
    } else {
      response = await api.editEvent(state.event?.editEventToJson());
    }
    if (response['success'] == true) {
      calendarBloc!.add(GetCalendarDataEvent());

      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'Your event "${state.event?.name}" successfully added!',
      ));
    } else {
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.error,
        message: 'Please try agin later',
      ));
    }
  }

  _deletEvent(DeleteEventForm event, Emitter<EventCreationState> emit) async {
    var response = await api.deleteEvent(eventId: state.event?.id);
    if (response['success'] == true) {
      calendarBloc!.add(GetCalendarDataEvent());
      snackBarKey.currentState?.showSnackBar(plannerSnackBar(
        snackBarType: SnackBarType.success,
        message: 'You successfully delete event',
      ));
    }
    emit(state.copyWith(isLoading: false));
  }

  _onChangeName(UpdateNameEvent event, Emitter<EventCreationState> emit) async {
    if (state.event != null) {
      final updatedEvent = state.event!.copyWith(name: event.newName);
      emit(state.copyWith(event: updatedEvent));
    }
  }

  _onChangeDescription(
      UpdateDescriptionEvent event, Emitter<EventCreationState> emit) async {
    if (state.event != null) {
      final updatedEvent =
          state.event!.copyWith(description: event.newDescription);
      emit(state.copyWith(event: updatedEvent));
    }
  }

  _onChangeGroup(
      UpdateGroupEvent event, Emitter<EventCreationState> emit) async {
    if (state.event != null) {
      final updatedEvent = state.event!.copyWith(group: event.newGroupId);
      emit(state.copyWith(event: updatedEvent));
    }
  }

  // _onAddGroup(AddNewGroupEvent event, Emitter<EventCreationState> emit) async {
  //   //todo change to show on active "choose group"
  // }

  _onChangeStartDate(
      UpdateStartTimeEvent event, Emitter<EventCreationState> emit) async {
    if (state.event != null) {
      if (state.event?.dateEnd != null) {
        if (event.newStartTime.isAfter(parseDate(state.event!.dateEnd!)
            .subtract(const Duration(hours: 1)))) {
          var updatedEnd = event.newStartTime.add(const Duration(hours: 1));
          add(UpdateEndTimeEvent(newEndTime: updatedEnd));
        }
      }
      var result = parseStringFromDate(event.newStartTime);
      final updatedEvent = state.event!.copyWith(dateStart: result);
      emit(state.copyWith(event: updatedEvent));
    }
  }

  _onChangeEndDate(
      UpdateEndTimeEvent event, Emitter<EventCreationState> emit) async {
    if (state.event != null) {
      var result = parseStringFromDate(event.newEndTime);
      final updatedEvent = state.event!.copyWith(dateEnd: result);
      emit(state.copyWith(event: updatedEvent));
    }
  }
}
