part of 'event_creation_bloc.dart';

abstract class EventCreationEvent {}

class InitEvent extends EventCreationEvent {}

class UpdateEvent extends EventCreationEvent {
  final CalendarEventModel event;

  UpdateEvent({required this.event});
}

class SubmitEventForm extends EventCreationEvent {}

class DeleteEventForm extends EventCreationEvent {}

class UpdateNameEvent extends EventCreationEvent {
  final String newName;

  UpdateNameEvent({required this.newName});
}

class UpdateDescriptionEvent extends EventCreationEvent {
  final String newDescription;

  UpdateDescriptionEvent({required this.newDescription});
}

class UpdateGroupEvent extends EventCreationEvent {
  final int newGroupId;

  UpdateGroupEvent({required this.newGroupId});
}

class UpdateStartTimeEvent extends EventCreationEvent {
  final DateTime newStartTime;

  UpdateStartTimeEvent({required this.newStartTime});
}

class UpdateEndTimeEvent extends EventCreationEvent {
  final DateTime newEndTime;

  UpdateEndTimeEvent({required this.newEndTime});
}
