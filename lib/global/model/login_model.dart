class LoginModel {
  String? email;
  String? password;

  LoginModel({
    this.email,
    this.password,
  });

  LoginModel.fromJson(dynamic json) {
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    json['email'] = email;
    json['password'] = password;
    return json;
  }

  LoginModel loginUser({
    String email = '',
    String password = '',
  }) {
    return LoginModel(
      email: email,
      password: password,
    );
  }
}
