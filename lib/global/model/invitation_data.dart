class InvitationData {
  final int id;
  final int groupId;
  final int guestId;
  final int invitorId;
  final String email;
  final bool? acepted;
  final String createdAt;
  final String updatedAt;

  InvitationData({
    required this.id,
    required this.groupId,
    required this.guestId,
    required this.invitorId,
    required this.email,
    required this.acepted,
    required this.createdAt,
    required this.updatedAt,
  });

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'group_id': groupId,
      'guest_id': guestId,
      'invitor_id': invitorId,
      'email': email,
      'accepted': acepted ?? false,
      'created_at': createdAt,
      'updated_at': updatedAt,
    };
  }

  factory InvitationData.fromJson(Map<String, dynamic> json) {
    return InvitationData(
      id: json['id'] as int,
      groupId: json['group_id'] as int,
      guestId: json['guest_id'] as int,
      invitorId: json['invitor_id'] as int,
      email: json['email'] as String,
      acepted: json['accepted'] as bool?,
      createdAt: json['created_at'] as String,
      updatedAt: json['updated_at'] as String,
    );
  }
}
