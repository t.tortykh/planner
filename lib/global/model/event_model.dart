class CalendarEventModel {
  int? id;
  String? name;
  String? description;
  int? group;
  String? dateStart;
  String? dateEnd;
  int? userId;

  CalendarEventModel(
      {this.id,
      this.name,
      this.description,
      this.group,
      this.dateStart,
      this.dateEnd,
      this.userId});

  CalendarEventModel newEvent({
    String? name,
    String? description,
    int? group,
    String? dateStart,
    String? dateEnd,
  }) {
    return CalendarEventModel(
        name: name ?? this.name,
        description: description ?? this.description,
        group: group ?? this.group,
        dateStart: dateStart ?? this.dateStart,
        dateEnd: dateEnd ?? this.dateEnd);
  }

  CalendarEventModel copyWith({
    int? id,
    String? name,
    String? description,
    int? group,
    String? dateStart,
    String? dateEnd,
    int? userId,
  }) {
    return CalendarEventModel(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        group: group ?? this.group,
        dateStart: dateStart ?? this.dateStart,
        dateEnd: dateEnd ?? this.dateEnd,
        userId: userId ?? this.userId);
  }

  Map<String, dynamic> newEventToJson() => {
        'name': name,
        'description': description,
        'group': group,
        'date_start': dateStart,
        'date_end': dateEnd
      };

  Map<String, dynamic> editEventToJson() => {
        'id': id,
        'name': name,
        'description': description,
        'group': group,
        'date_start': dateStart,
        'date_end': dateEnd,
        // 'user_id': userId
      };

  CalendarEventModel.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    group = json['group'];
    dateStart = json['date_start'];
    dateEnd = json['date_end'];
    userId = json['user_id'];
  }
}
