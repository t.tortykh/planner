class User {
  final int id;
  final String name;
  final String? lastName;
  final String email;
  final String? iconUrl;

  User({
    required this.id,
    required this.name,
    required this.lastName,
    required this.email,
    required this.iconUrl,
  });

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'first_name': name,
      'last_name': lastName,
      'email': email,
      'icon_url': iconUrl
    };
  }

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'] as int,
      name: json['first_name'] as String,
      lastName: json['last_name'] as String,
      email: json['email'] as String,
      iconUrl: json['icon_url'] as String?,
    );
  }
}
