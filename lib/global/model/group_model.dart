import 'package:planner/global/model/invitation_data.dart';
import 'package:planner/global/model/user.dart';

class GroupModel {
  int? id;
  String? name;
  String? color;
  String? description;
  bool? isOwner;
  int? ownerId;
  List<User>? participants;
  InvitationData? invitationData;

  GroupModel({
    this.id,
    this.name,
    this.color,
    this.isOwner,
    this.description,
    this.ownerId,
    this.participants,
    this.invitationData,
  });

  GroupModel newGroup({
    String? name,
    String? color,
    String? description,
  }) {
    return GroupModel(
      name: name ?? this.name,
      color: color ?? this.color,
      description: description ?? this.description,
    );
  }

  Map<String, dynamic> newGroupToJson() =>
      {'name': name, 'color': color, 'description': description};

  Map<String, dynamic> groupToJson() => {
        'id': id,
        'name': name,
        'color': color,
        'description': description,
        'user_id': ownerId,
        'participants': participants,
        // 'invitationData': invitationData,
      };

  GroupModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    name = json['name'] as String;
    color = json['color'];
    isOwner = json['is_owner'];
    description = json['description'];
    ownerId = json['user_id'];
    if (json['participants'] != null) {
      var participantsList = json['participants'] as List<dynamic>;
      participants = participantsList.map((userJson) {
        return User.fromJson(userJson as Map<String, dynamic>);
      }).toList();
    }
    // participants = List<User>.from(json['participants'].map((user) => User.fromJson(user)));
    final invitationDataJson = json['invitationData'];
    if (invitationDataJson != null) {
      invitationData = InvitationData.fromJson(invitationDataJson);
    }
  }
}
