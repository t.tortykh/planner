class UserSignUpRequest {
  String? firstName;
  String? lastName;
  String? email;
  String? password;

  UserSignUpRequest({
    this.firstName,
    this.lastName,
    this.email,
    this.password,
  });

  UserSignUpRequest newUser({
    String? firstName,
    String? lastName,
    String? email,
    String? password,
  }) {
    return UserSignUpRequest(
        firstName: firstName ?? this.firstName,
        lastName: lastName ?? this.lastName,
        email: email ?? this.email,
        password: password ?? this.password);
  }

  UserSignUpRequest.fromJson(dynamic json) {
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    json['first_name'] = firstName;
    json['last_name'] = lastName;
    json['email'] = email;
    json['password'] = password;
    return json;
  }
}
