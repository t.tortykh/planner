import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:planner/global/model/user.dart';
import 'package:planner/presentation/modules/groups/view/groups_screen.dart';

import '../model/event_model.dart';
import '../model/group_model.dart';

User? firstUserOfGroup({required GroupModel group, required User user}) {
  return group.participants?.firstWhere((element) => element.id != user.id);
}

bool isUserPartisipant({required GroupModel group, required User user}) {
  bool isCurrentUserParticipant = false;
  if (group.participants != null) {
    for (var participant in group.participants!) {
      if (participant.id == user.id) {
        isCurrentUserParticipant = true;
        break;
      }
    }
  }
  return isCurrentUserParticipant;
}

String getOwnerName(GroupModel group) {
  String ownerName = '';
  if (group.participants != null) {
    for (var user in group.participants!) {
      if (group.ownerId == user.id) {
        ownerName = '${user.name} ${user.lastName}';
      }
    }
  }
  return ownerName;
}

List<User> participantsList(
    List<User> users, int number, GroupsType groupType, User user) {
  List<User> participants = [];
  var count = number > 0 ? number : 4;
  bool isUserPresent = groupType == GroupsType.groupsList;
  count = isUserPresent ? count - 1 : count;

  if (isUserPresent) {
    participants.add(user);
  }

  for (var i = 0; i <= count; i++) {
    if (i > users.length - 1) break;
    if (users[i].id != user.id) {
      participants.add(users[i]);
    }
  }

  return participants;
}

parseColor(String colorString) {
  final startIndex = colorString.indexOf('0x');
  final endIndex = colorString.length - 1;
  final hexString = colorString.substring(startIndex, endIndex);
  return Color(int.parse(hexString));
}

ButtonStyle dateTimeButtonStyle() {
  return ElevatedButton.styleFrom(
    padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 20),
    backgroundColor: const Color.fromARGB(248, 2, 70, 37),
    textStyle: const TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.bold,
      fontSize: 15,
    ),
    shape: const RoundedRectangleBorder(
      side: BorderSide(width: 2, color: Color.fromRGBO(34, 197, 94, 1)),
      borderRadius: BorderRadius.all(
        Radius.circular(25.0),
      ),
    ),
  );
}

Color getGroupColor(
    {required List<GroupModel> groupsList, required CalendarEventModel event}) {
  Color color = Colors.grey;
  for (GroupModel groupData in groupsList) {
    if (event.group != null) {
      if (event.group == groupData.id) {
        if (groupData.color != null) {
          color = parseColor(groupData.color!);
        }
        break;
      }
    }
  }
  return color;
}

String getGroupName(
    {required List<GroupModel> groupsList, required CalendarEventModel event}) {
  String name = '';
  for (GroupModel groupData in groupsList) {
    if (event.group != null) {
      if (event.group == groupData.id) {
        if (groupData.name != null) {
          name = groupData.name!;
        }
        break;
      }
    }
  }
  return name;
}

Widget getGroups({
  List<GroupModel>? groupsList,
  CalendarEventModel? event,
  required BuildContext context,
}) {
  Color color = Colors.grey;
  String name = '';
  if (groupsList != null && event != null) {
    for (GroupModel groupData in groupsList) {
      if (event.group != null) {
        if (event.group == groupData.id) {
          name = groupData.name ?? '';
          if (groupData.color != null) {
            color = parseColor(groupData.color!);
          }
          break;
        }
      }
    }
  }
  return Row(
    children: [
      Expanded(
        flex: 1,
        child: Container(
          color: color,
          height: 13,
          width: 13,
        ),
      ),
      const SizedBox(width: 3),
      Expanded(
        flex: 6,
        child: Text(
          name,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.bodySmall,
        ),
      )
    ],
  );
}

Widget getStartTime(
    {required CalendarEventModel event, required BuildContext context}) {
  return Text(
    event.dateStart == event.dateEnd
        ? 'All day'
        : DateFormat('Hm').format(DateTime.parse(event.dateStart!)).toString(),
    style: Theme.of(context).textTheme.bodySmall,
  );
}

DateTime parseDate(String date) {
  DateTime dateTime = DateTime.parse(date);
  return dateTime;
}

String parseStringFromDate(DateTime date) {
  var dateString = DateFormat('yyyy-MM-dd HH:mm:ss').format(date);
  return dateString;
}

bool isTimeBefore(TimeOfDay time1, TimeOfDay time2) {
  if (time1.hour < time2.hour) {
    return true;
  } else if (time1.hour == time2.hour) {
    return time1.minute < time2.minute;
  } else {
    return false;
  }
}

TimeOfDay roundTime(TimeOfDay time) {
  int totalMinutes = time.hour * 60 + time.minute;
  int roundedMinutes = ((totalMinutes + 7) ~/ 15) * 15;
  int roundedHours = roundedMinutes ~/ 60;
  roundedMinutes = roundedMinutes % 60;
  return TimeOfDay(hour: roundedHours, minute: roundedMinutes);
}

class FormDivider extends StatelessWidget {
  const FormDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return const Divider(
      height: 1,
      thickness: 3,
      color: Color.fromARGB(255, 191, 121, 121),
    );
  }
}
