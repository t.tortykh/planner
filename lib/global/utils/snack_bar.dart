import 'package:flutter/material.dart';

enum SnackBarType { error, success, info }

SnackBar plannerSnackBar({required String message, required SnackBarType snackBarType}) {
  String title;
  Color color;
  String imagePath;
  switch (snackBarType) {
    case SnackBarType.success:
      title = 'Done!';
      color = Colors.green;
      imagePath = 'lib/global/assets/check.png';
    case SnackBarType.error:
      title = 'Something went wrong';
      color = Colors.red;
      imagePath = 'lib/global/assets/danger.png';
    case SnackBarType.info:
      title = 'Info';
      color = Colors.yellow;
      imagePath = 'lib/global/assets/info.png';
  }
  return SnackBar(
    behavior: SnackBarBehavior.floating,
    backgroundColor: Colors.transparent,
    elevation: 0,
    content: Container(
      padding: const EdgeInsets.all(2),
      height: 90,
      decoration: BoxDecoration(
        border: Border.all(
          color: const Color.fromARGB(255, 83, 83, 83),
          width: 1.5,
        ),
        color: const Color.fromARGB(255, 50, 50, 50),
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 5,
              decoration: BoxDecoration(
                color: color,
                borderRadius: const BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
            ),
            const SizedBox(width: 7),
            CircleAvatar(
              backgroundColor: color.withOpacity(0.2),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(
                  imagePath,
                  color: color,
                ),
              ),
            ),
            const SizedBox(width: 7),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                    ),
                  ),
                  Text(
                    message,
                    style: const TextStyle(
                      color: Color.fromARGB(255, 192, 192, 192),
                      fontSize: 10,
                    ),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 4,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
