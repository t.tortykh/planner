enum InputType {
  firstName('first_name'),
  lastName('last_name'),
  email('email'),
  password('password');

  final String title;
  const InputType(this.title);
}

List defineError({required Map<String, dynamic> validationMsges, required String type}) {
  var errorsList = [];
  validationMsges.forEach((k, v) {
    if (k == type) {
      errorsList = v;
    }
  });
  return errorsList;
}
