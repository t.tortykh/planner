mixin UseValidation {
  RegExp emailRegEx = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
  RegExp pswRegEx = RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');

  validateEmail(value) {
    if (value?.isEmpty ?? true) {
      return 'Please enter email';
    } else if (!emailRegEx.hasMatch(value!)) {
      return 'Check email';
    } else {
      return null;
    }
  }

  validatePassword(value) {
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (!pswRegEx.hasMatch(value)) {
        return 'Enter valid password';
      } else {
        return null;
      }
    }
  }

  requiredField(value) {
    if (value == null || value.isEmpty) {
      return 'This field is required';
    }
    return null;
  }

  matchPasswords({String? password, String? value}) {
    if (value != password) {
      return 'The passwords didn\'t match';
    } else {
      return null;
    }
  }
}
