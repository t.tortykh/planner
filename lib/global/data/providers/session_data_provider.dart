import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:planner/global/model/user.dart';

abstract class _Keys {
  static const token = 'token';
  // static const avatarPath = 'imgPath';
  static const user = 'user';
  static const userName = 'userName';
}

class SessionDataProvider {
  static const _secureStorage = FlutterSecureStorage(
    aOptions: AndroidOptions(encryptedSharedPreferences: true),
  );

  Future<String?> getToken() => _secureStorage.read(key: _Keys.token);
  Future<void> setToken(value) {
    if (value != null) {
      return _secureStorage.write(key: _Keys.token, value: value);
    } else {
      return _secureStorage.delete(key: _Keys.token);
    }
  }

  Future<void> setUser(User? user) async {
    if (user != null) {
      final userJson = jsonEncode(user.toJson());
      await _secureStorage.write(key: _Keys.user, value: userJson);
    } else {
      await _secureStorage.delete(key: _Keys.user);
    }
  }

  Future<User?> getUser() async {
    final userJson = await _secureStorage.read(key: _Keys.user);
    if (userJson != null) {
      final Map<String, dynamic> userMap = jsonDecode(userJson);
      return User.fromJson(userMap);
    }
    return null;
  }

  Future<String?> getUserName() => _secureStorage.read(key: _Keys.userName);
  Future<void> setUserName(value) {
    if (value != null) {
      return _secureStorage.write(key: _Keys.userName, value: value);
    } else {
      return _secureStorage.delete(key: _Keys.userName);
    }
  }

  clearUserData() async {
    await setToken(null);
    await setUserName(null);
    await setUser(null);
  }
}
